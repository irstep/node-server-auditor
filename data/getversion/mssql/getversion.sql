SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

-- SELECT @@VERSION AS [ServerVersion];
SELECT CONVERT([NVARCHAR](128), SERVERPROPERTY(N'ProductVersion')) AS [ServerVersion];
