SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@EventDateTime [DATETIME]
;

SET @EventDateTime = GETDATE();

SELECT
	 @EventDateTime AS [EventDateTime]
	,@@ServerName   AS [ServerName]
	,tDB.[dbid]     AS [DatabaseId]
	,tDB.[name]     AS [DatabaseName]
	,0.0            AS [DatabaseSizeMB]
	,0.0            AS [DatabaseDataSizeMB]
	,0.0            AS [DatabaseDataSizeUsedMB]
	,0.0            AS [DatabaseLogSizeMB]
	,0.0            AS [DatabaseLogSizeUsedMB]
FROM
	[master].[dbo].[sysdatabases] tDB
WHERE
	DatabasePropertyEx(tDB.[name], N'Status') IN (
		N'ONLINE'
	)
	AND tDB.[name] NOT IN (
		N'model'
	)
GROUP BY
	 tDB.[dbid]
	,tDB.[name]
ORDER BY
	tDB.[name] ASC
;
