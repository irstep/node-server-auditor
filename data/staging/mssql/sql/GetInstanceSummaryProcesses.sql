SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intMySpId [INTEGER]
;

DECLARE
	@tblProcesses TABLE
	(
		 [EventDateTime] [DATETIME]      NOT NULL
		,[SpId]          [INTEGER]       NOT NULL
		,[Loginame]      [NVARCHAR](128) NOT NULL
		,[Cpu]           [BIGINT]        NOT NULL
		,[PhysicalIO]    [BIGINT]        NOT NULL
		,[Blocked]       [INTEGER]       NOT NULL
		,[Status]        [NVARCHAR](128) NOT NULL
	);

SET @intMySpId = @@spid;

INSERT INTO @tblProcesses
(
	 [EventDateTime]
	,[SpId]
	,[Loginame]
	,[Cpu]
	,[PhysicalIO]
	,[Blocked]
	,[Status]
)
SELECT
	 getdate()                                            AS [EventDateTime]
	,ISNULL(tSP.[spid], 0)                                AS [SpId]
	,LTRIM(RTRIM(ISNULL(tSP.[loginame], N'?')))           AS [Loginame]
	,SUM(ISNULL(CONVERT([BIGINT], tSP.[cpu]), 0))         AS [Cpu]
	,SUM(ISNULL(CONVERT([BIGINT], tSP.[physical_io]), 0)) AS [PhysicalIO]
	,ISNULL(tSP.[blocked], 0)                             AS [Blocked]
	,LTRIM(RTRIM(ISNULL(tSP.[Status], N'?')))             AS [Status]
FROM
	[master].[dbo].[sysprocesses] tSP
WHERE
	tSP.[spid] NOT IN (@intMySpId)
GROUP BY
	 ISNULL(tSP.[spid], 0)
	,LTRIM(RTRIM(ISNULL(tSP.[loginame], N'?')))
	,ISNULL(tSP.[blocked], 0)
	,LTRIM(RTRIM(ISNULL(tSP.[Status], N'?')))
;

SELECT
	 MAX(t.[EventDateTime])     AS [EventDateTime]
	,@@ServerName               AS [ServerName]
	,SUM(CASE
		WHEN (t.[spid] <= 51 AND t.[blocked] = 0) THEN
			1
		ELSE
			0
	END)                        AS [NumberOfSystemProcesses]
	,SUM(CASE
		WHEN (t.[spid] > 51 AND t.[blocked] = 0 AND t.[Status] IN (N'sleeping', N'background', N'dormant')) THEN
			1
		ELSE
			0
	END)                        AS [NumberOfUserProcesses]
	,SUM(CASE
		WHEN (t.[spid] > 51 AND t.[blocked] = 0 AND t.[Status] NOT IN (N'sleeping', N'background', N'dormant')) THEN
			1
		ELSE
			0
	END)                        AS [NumberOfActiveUserProcesses]
	,SUM(CASE
		WHEN (t.[blocked] > 0) THEN
			1
		ELSE
			0
	END)                        AS [NumberOfBlockedProcesses]
FROM
	@tblProcesses t
;
