INSERT INTO [dwh_InstanceIOStatus]
(
	 [EventDateTime]
	,[InstanceConnectionId]
	,[Disk]
	,[DatabaseId]
	,[DatabaseName]
	,[num_of_reads]
	,[num_of_bytes_read]
	,[io_stall_read_ms]
	,[num_of_writes]
	,[num_of_bytes_written]
	,[io_stall_write_ms]
	,[io_stall]
)
SELECT
	 t.[EventDateTime]
	,t.[_id_connection]
	,t.[Disk]
	,t.[DatabaseId]
	,t.[DatabaseName]
	,t.[num_of_reads]
	,t.[num_of_bytes_read]
	,t.[io_stall_read_ms]
	,t.[num_of_writes]
	,t.[num_of_bytes_written]
	,t.[io_stall_write_ms]
	,t.[io_stall]
FROM
	[${getInstanceIOStatus}{0}$] t
	LEFT OUTER JOIN [dwh_InstanceIOStatus] dIIS ON
		dIIS.[EventDateTime] = t.[EventDateTime]
		AND dIIS.[InstanceConnectionId] = t.[_id_connection]
		AND dIIS.[Disk] = t.[Disk]
		AND dIIS.[DatabaseId] = t.[DatabaseId]
		AND dIIS.[DatabaseName] = t.[DatabaseName]
WHERE
	t.[_id_connection] = $_id_connection
	AND dIIS.[InstanceConnectionId] IS NULL
;
