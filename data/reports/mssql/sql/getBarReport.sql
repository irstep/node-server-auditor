SET NOCOUNT ON;
SET DEADLOCK_PRIORITY LOW;

DECLARE
	@intIndex [INTEGER]
;

CREATE TABLE #t1
(
	 [RecordSet] [INTEGER]        NOT NULL
	,[Name]      [NVARCHAR](128)  NOT NULL
	,[Value]     [DECIMAL](24, 4) NOT NULL
);

SET @intIndex = 1;

WHILE (@intIndex <= 10)
BEGIN
	INSERT INTO #t1
	(
		 [RecordSet]
		,[Name]
		,[Value]
	)
	VALUES
	(
		 1
		,RIGHT(N'000000' + CONVERT([NVARCHAR](128), @intIndex), 6)
		,-10.0 + 20.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SET @intIndex = 1;

WHILE (@intIndex <= 20)
BEGIN
	INSERT INTO #t1
	(
		 [RecordSet]
		,[Name]
		,[Value]
	)
	VALUES
	(
		 2
		,RIGHT(N'000000' + CONVERT([NVARCHAR](128), @intIndex), 6)
		,-20.0 + 40.0 * RAND()
	);

	SET @intIndex = @intIndex + 1;
END

SELECT
	 t.[Name]  AS [Name]
	,t.[Value] AS [Value]
FROM
	#t1 t
WHERE
	t.[RecordSet] = 1
ORDER BY
	t.[Name] ASC
;

SELECT
	 t.[Name]  AS [Name]
	,t.[Value] AS [Value]
FROM
	#t1 t
WHERE
	t.[RecordSet] = 2
ORDER BY
	t.[Name] ASC
;
