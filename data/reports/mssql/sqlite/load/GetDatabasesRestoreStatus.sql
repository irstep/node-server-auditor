SELECT
	 t.[DatabaseId]               AS [DatabaseId]
	,t.[DatabaseName]             AS [DatabaseName]
	,t.[DatabaseStatus]           AS [DatabaseStatus]
	,t.[DatabaseIsInStandby]      AS [DatabaseIsInStandby]
	,t.[DatabaseRestoreType]      AS [DatabaseRestoreType]
	,t.[DatabaseRestoredDateTime] AS [DatabaseRestoredDateTime]
	,t.[DatabaseRestoredStatus]   AS [DatabaseRestoredStatus]
	,t.[SourceServerName]         AS [SourceServerName]
	,t.[SourceDatabaseName]       AS [SourceDatabaseName]
	,t.[SourceBackupFinishDate]   AS [SourceBackupFinishDate]
	,t.[SourceBackupSizeMB]       AS [SourceBackupSizeMB]
	,t.[SourceBackupFileName]     AS [SourceBackupFileName]
FROM
	[${GetDatabasesRestoreStatus}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
;
