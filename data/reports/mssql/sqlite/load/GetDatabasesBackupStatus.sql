SELECT
	 t.[DatabaseId]                   AS [DatabaseId]
	,t.[DatabaseName]                 AS [DatabaseName]
	,t.[DatabaseStatus]               AS [DatabaseStatus]
	,t.[DatabaseUpdateability]        AS [DatabaseUpdateability]
	,t.[DatabaseBackupIsSnapshot]     AS [DatabaseBackupIsSnapshot]
	,t.[DatabaseBackupType]           AS [DatabaseBackupType]
	,t.[DatabaseBackupName]           AS [DatabaseBackupName]
	,t.[DatabaseBackupDateTime]       AS [DatabaseBackupDateTime]
	,t.[DatabaseBackupSize]           AS [DatabaseBackupSize]
	,t.[DatabaseCompressedBackupSize] AS [DatabaseCompressedBackupSize]
	,t.[DatabaseBackupDevice]         AS [DatabaseBackupDevice]
FROM
	[${GetDatabasesBackupStatus}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
;
