SELECT
	 t.[Name]  AS [Name]
	,t.[Value] AS [Value]
FROM
	[${getBarReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[Name] ASC
;
