SELECT
	 t.[EventDateTime] AS [EventDateTime]
	,t.[EventValue1]   AS [EventValue1]
	,t.[EventValue2]   AS [EventValue2]
FROM
	[${getTwoDatetimeValueReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[EventDateTime] DESC
;
