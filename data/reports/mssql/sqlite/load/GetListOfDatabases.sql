SELECT
	 t.[ServerName]           AS [ServerName]
	,t.[DatabaseId]           AS [DatabaseId]
	,t.[DatabaseName]         AS [DatabaseName]
	,t.[DatabaseCreationDate] AS [DatabaseCreationDate]
	,t.[DatabaseOwner]        AS [DatabaseOwner]
	,t.[DatabaseCollation]    AS [DatabaseCollation]
FROM
	[${GetListOfDatabases}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	 t.[ServerName] ASC
	,t.[DatabaseId] ASC
	,t.[DatabaseName] ASC
;
