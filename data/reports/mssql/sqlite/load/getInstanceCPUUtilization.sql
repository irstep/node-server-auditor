SELECT
	 t.[ServerName]                   AS [ServerName]
	,t.[EventTime]                    AS [EventTime]
	,t.[RecordTimeStamp]              AS [RecordTimeStamp]
	,t.[RecordRingBufferType]         AS [RecordRingBufferType]
	,t.[MemoryUtilization]            AS [MemoryUtilization]
	,t.[SystemIdle]                   AS [SystemIdle]
	,t.[SQLProcessCPUUtilization]     AS [SQLProcessCPUUtilization]
	,t.[OtherProcessesCPUUtilization] AS [OtherProcessesCPUUtilization]
FROM
	[${getInstanceCPUUtilization}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[EventTime] DESC
;
