SELECT
	 t.[ServerName]             AS [ServerName]
	,t.[DatabaseId]             AS [DatabaseId]
	,t.[DatabaseName]           AS [DatabaseName]
	,t.[DatabaseSizeMB]         AS [DatabaseSizeMB]
	,t.[DatabaseDataSizeMB]     AS [DatabaseDataSizeMB]
	,t.[DatabaseDataSizeUsedMB] AS [DatabaseDataSizeUsedMB]
	,t.[DatabaseLogSizeMB]      AS [DatabaseLogSizeMB]
	,t.[DatabaseLogSizeUsedMB]  AS [DatabaseLogSizeUsedMB]
FROM
	[${GetSizeOfDatabases}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	 t.[ServerName] ASC
	,t.[DatabaseId] ASC
	,t.[DatabaseName] ASC
;
