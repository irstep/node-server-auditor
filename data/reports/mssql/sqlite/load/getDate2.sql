SELECT
	 t.[ServerName]      AS [ServerName]
	,t.[ServerVersion]   AS [ServerVersion]
	,t.[SUserName]       AS [SUserName]
	,t.[UserName]        AS [UserName]
	,t.[EventTime]       AS [EventTime]
	,t.[Message]         AS [Message]
	,t.[RecordSet]       AS [RecordSet]
FROM
	[${getDate2}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[ServerName] ASC
;
