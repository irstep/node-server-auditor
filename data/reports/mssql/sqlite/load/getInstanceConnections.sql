SELECT
	 t.[SessionID]                      AS [SessionID]
	,t.[SessionLoginName]               AS [SessionLoginName]
	,t.[SessionConnectTime]             AS [SessionConnectTime]
	,t.[SessionAuthScheme]              AS [SessionAuthScheme]
	,t.[SessionNetTransport]            AS [SessionNetTransport]
	,t.[SessionProtocolType]            AS [SessionProtocolType]
	,t.[SessionClientNetAddress]        AS [SessionClientNetAddress]
	,t.[SessionHostName]                AS [SessionHostName]
	,t.[SessionClientInterfaceName]     AS [SessionClientInterfaceName]
	,t.[RequestCommand]                 AS [RequestCommand]
	,t.[RequestStartTime]               AS [RequestStartTime]
	,t.[RequestPercentComplete]         AS [RequestPercentComplete]
	,t.[RequestSecondsToGo]             AS [RequestSecondsToGo]
	,t.[RequestEstimatedCompletionTime] AS [RequestEstimatedCompletionTime]
FROM
	[${getInstanceConnections}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[SessionID] ASC
;
