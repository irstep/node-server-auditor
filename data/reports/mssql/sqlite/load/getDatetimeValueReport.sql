SELECT
	 t.[EventDateTime] AS [EventDateTime]
	,t.[EventValue]    AS [EventValue]
FROM
	[${getDatetimeValueReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[EventDateTime] DESC
;
