SELECT
	 t.[ServerName]     AS [ServerName]
	,t.[LoginName]      AS [LoginName]
	,t.[CreateDateTime] AS [CreateDateTime]
	,t.[IsHasAccess]    AS [IsHasAccess]
	,t.[IsDenyLogin]    AS [IsDenyLogin]
	,t.[LoginLanguage]  AS [LoginLanguage]
FROM
	[${GetListOfLogins}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	 t.[ServerName] ASC
	,t.[LoginName] ASC
;
