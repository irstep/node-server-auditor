SELECT
	 t.[idLine]  AS [idLine]
	,t.[idPoint] AS [idPoint]
	,t.[XValue]  AS [XValue]
	,t.[YValue]  AS [YValue]
FROM
	[${getTwoXYLinesReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	 t.[idLine] ASC
	,t.[idPoint] ASC
;
