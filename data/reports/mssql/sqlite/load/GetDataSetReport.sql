SELECT
	 t.[Name]   AS [Name]
	,t.[Value1] AS [Value1]
	,t.[Value2] AS [Value2]
	,t.[Value3] AS [Value3]
FROM
	[${GetDataSetReport}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[Name] ASC
;
