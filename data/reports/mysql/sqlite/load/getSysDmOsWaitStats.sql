SELECT
	 t.[EventTime]  AS [EventTime]
	,t.[WaitType]   AS [WaitType]
	,t.[WaitTimeMs] AS [WaitTimeMs]
FROM
	[${getSysDmOsWaitStats}{0}$] t
WHERE
	t.[_id_connection] = $_id_connection
ORDER BY
	t.[EventTime] DESC
;
