SELECT
	 'A'          AS `Name`
	,RAND() * 100 AS `Value1`

UNION ALL

SELECT
	 'B'          AS `Name`
	,RAND() * 100 AS `Value1`
;

SELECT
	 'A'          AS `Name`
	,RAND() * 100 AS `Value1`
	,RAND() * 200 AS `Value2`
	,RAND() * 300 AS `Value3`

UNION ALL

SELECT
	 'B'          AS `Name`
	,RAND() * 100 AS `Value1`
	,RAND() * 200 AS `Value2`
	,RAND() * 300 AS `Value3`

UNION ALL

SELECT
	 'C'          AS `Name`
	,RAND() * 100 AS `Value1`
	,RAND() * 200 AS `Value2`
	,RAND() * 300 AS `Value3`
;
