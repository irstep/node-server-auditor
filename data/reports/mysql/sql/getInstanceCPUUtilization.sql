SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 8 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 8 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 50                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 7 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 7 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 50                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 6 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 6 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 50                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 5 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 5 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 50                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 4 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 4 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 40                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 3 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 3 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 30                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 2 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 2 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 20                                              AS `OtherProcessesCPUUtilization`

UNION ALL

SELECT
	 @@hostname                                               AS `ServerName`
	,DATE_FORMAT(now() - INTERVAL 1 MINUTE, '%Y-%m-%d %H:%i') AS `EventTime`
	,DATE_FORMAT(now() - INTERVAL 1 MINUTE, '%Y-%m-%d %H:%i') AS `RecordTimeStamp`
	,'A'                                                      AS `RecordRingBufferType`
	,10                                                       AS `MemoryUtilization`
	,20                                                       AS `SystemIdle`
	,RAND() * 100                                             AS `SQLProcessCPUUtilization`
	,RAND() * 20                                              AS `OtherProcessesCPUUtilization`
;
