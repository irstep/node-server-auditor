CREATE TABLE IF NOT EXISTS [t_staging_test_01]
(
	 [DateCreated]    [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]    [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[name]           [TEXT]     NOT NULL
	,[value]          [INTEGER]  NOT NULL
);
