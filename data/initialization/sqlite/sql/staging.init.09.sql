CREATE TABLE IF NOT EXISTS [dwh_InstanceIOStatus]
(
	 [DateCreated]                  [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]                  [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[EventDateTime]                [DATETIME] NOT NULL
	,[InstanceConnectionId]         [INTEGER]  NOT NULL
	,[Disk]                         [TEXT]     NOT NULL
	,[DatabaseId]                   [INTEGER]  NOT NULL
	,[DatabaseName]                 [TEXT]     NOT NULL
	,[num_of_reads]                 [INTEGER]  NULL
	,[num_of_bytes_read]            [INTEGER]  NULL
	,[io_stall_read_ms]             [INTEGER]  NULL
	,[num_of_writes]                [INTEGER]  NULL
	,[num_of_bytes_written]         [INTEGER]  NULL
	,[io_stall_write_ms]            [INTEGER]  NULL
	,[io_stall]                     [INTEGER]  NULL
);
