CREATE INDEX IF NOT EXISTS [idx_dwh_InstanceCPUUtilization] ON [dwh_InstanceCPUUtilization]
(
	 [EventDateTime]
	,[InstanceConnectionId]
);
