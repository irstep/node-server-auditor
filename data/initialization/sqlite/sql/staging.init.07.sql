CREATE TABLE IF NOT EXISTS [dwh_InstanceCPUUtilization]
(
	 [DateCreated]                  [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[DateUpdated]                  [DATETIME] NOT NULL DEFAULT (DATETIME('now'))
	,[EventDateTime]                [DATETIME] NOT NULL
	,[InstanceConnectionId]         [INTEGER]  NOT NULL
	,[RecordTimeStamp]              [INTEGER]  NULL
	,[RecordRingBufferType]         [TEXT]     NULL
	,[MemoryUtilization]            [INTEGER]  NULL
	,[SystemIdle]                   [INTEGER]  NULL
	,[SQLProcessCPUUtilization]     [INTEGER]  NULL
	,[OtherProcessesCPUUtilization] [INTEGER]  NULL
);
