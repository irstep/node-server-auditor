'use strict';

(function (module) {
	'use strict';

	var express             = require( 'express' );
	var logger              = require( 'src/node/log' );
	var Data                = require( 'src/node/com/data' );
	var Report              = require( 'src/node/com/report' );
	var ReportModuleFactory = require( 'src/node/com/report/reportModuleFactory' );
	var ServerConfiguration = require( 'src/node/com/report/serverConfiguration' );
	var router              = express.Router();
	var handler;

	router.get( '/', function( req, res ) {
	} );

	router.get( '/reports', function( req, res ) {
		ReportModuleFactory.getAvailableExtended().then( function( data ) {
			res.json( handler.response( {
				config:  Report(req.sessionId).isConfigured() ? Report(req.sessionId).config.data() : null,
				modules: data.modules,
				grupped: data.servers,
				servers: ServerConfiguration.getAvailable()
			} ) );
		} );
	});

	router.put( '/reports', function( req, res ) {
		// logger.debug( '[30]router.put( "/reports"' );
		// logger.debug( '[31]req: {', req, '}' );
		// logger.debug( '[32]res: {', res, '}' );

		var config       = null;
		var respConf     = null;
		var serverConfig = null;

		if ( !Object.keys(req.body).length ) {
			res.json(handler.response(Report( req.sessionId ).configure(null)));

			return;
		}

		// logger.debug( '[44]req.body.server: {', req.body.server, '}' );
		// logger.debug( '[45]req.body.group: {', req.body.group, '}' );

		serverConfig = ServerConfiguration.getConfiguration( req.body.server, req.body.group );

		// logger.debug( '[49]serverConfig: {', serverConfig, '}' );

		ServerConfiguration.getServerVersion( serverConfig, req.body.module, function( rVer ) {
			var ver = '0';

			if ( rVer.success ) {
				ver = rVer.ver;
			}

			// logger.debug( '[58]ver: {', ver, '}' );

			config = new Data.DB_Configuration({
				server:  serverConfig,
				module:  req.body.module,
				version: ver
			});

			// logger.debug( '[66]config: {', config, '}' );

			if ( config.isValid() ) {
				respConf = Report(req.sessionId).configure( config );

				res.json(handler.response(respConf));
			}
			else {
				res.json(handler.error( 'wrong server configuration' ));
			}
		} );
	} );

	module.exports = function( resHandler ) {
		handler = resHandler;

		return router;
	};
} )( module );
