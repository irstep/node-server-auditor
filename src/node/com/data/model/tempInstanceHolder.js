'use strict';

/**
 * Temporary Instance Holder
 *
 * Check instance for actual using 'actual' method.
 */
(function (module) {
	var util   = require('util');
	var extend = require('extend');
	var Model  = require('./');

	var TempInstanceHolder = function (instance, actualTime) {
		TempInstanceHolder.super_.apply(this, arguments);

		this.actualTime = actualTime;

		this.set('time', new Date());
		this.set('instance', instance);
	};

	util.inherits(TempInstanceHolder, Model);

	extend(TempInstanceHolder.prototype, {
		get: function (key) {
			if ( key === 'instance' ) {
				this.set('time', new Date());
			}

			return Model.prototype.get.apply(this, arguments);
		},

		/**
		 * Check if it is actual
		 */
		actual: function() {
			var lastUsedTime = this.get('time');
			var now          = new Date();

			return (now.getTime() - lastUsedTime.getTime()) < this.actualTime;
		}
	});

	module.exports = TempInstanceHolder;
})(module);
