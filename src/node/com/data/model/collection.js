'use strict';

(function (module, Object) {
	var util  = require('util');
	var Model = require('./');

	var Collection = function() {
		Collection.super_.apply(this, arguments);

		this.container    = [];
		this.requiredType = Object;
	};

	util.inherits(Collection, Model);

	Collection.prototype.add = function (item) {
		if (item instanceof this.requiredType) {
			this._push(item);
		}
		else {
			throw 'Wrong data type';
		}
	};

	Collection.prototype.map = function (fn) {
		this.container.map(fn);
	};

	Collection.prototype._push = function (item) {
		this.container.push(item);
	};

	module.exports = Collection;
})(module, Object);
