'use strict';

(function (module) {
	var util  = require('util');
	var Model = require('./');

	var JSON_Response = function (params) {
		JSON_Response.super_.apply(this, arguments);

		this.set('response', params.response);
		this.set('error',    params.error);
		this.set('data',     params.data || {});
	};

	util.inherits(JSON_Response, Model);

	module.exports = JSON_Response;
})(module);
