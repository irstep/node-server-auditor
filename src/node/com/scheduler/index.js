'use strict';

(function (module) {
	var Q                           = require( 'q' );
	var async                       = require( 'async' );
	var Promise                     = require( 'promise' );
	var _                           = require( 'lodash' );
	var nodeSchedule                = require( 'node-schedule' );
	var Report                      = require( 'src/node/com/report' );
	var logger                      = require( 'src/node/log' );
	var database                    = require( 'src/node/database')();
	var dbsched                     = require( 'src/node/database/model' );
	var t_group                     = require( 'src/node/database/model/t_group' );
	var t_connection                = require( 'src/node/database/model/t_connection' );
	var t_connection_query_schedule = require( 'src/node/database/model/t_connection_query_schedule' );
	var t_connection_type           = require( 'src/node/database/model/t_connection_type' );
	var t_module                    = require( 'src/node/database/model/t_module' );
	var t_report                    = require( 'src/node/database/model/t_report' );

	/**
	 * Report class
	 *
	 */
	function Scheduler() {
		// logger.debug( '[25]Scheduler.prototype.enable' );

		this._arr_sched = [];
	}

	/**
	 * Enable determined task
	 *
	 * @param {mixed} record It may be record Object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.enable = function( record, sessionId ) {
		// logger.debug( '[35]Scheduler.prototype.enable' );
		// logger.debug( '[36]record: {', record, '}' );
		// logger.debug( '[37]sessionId: {', sessionId, '}' );

		var active_schedule = null;
		var recent          = true;
		var dfd             = Q.defer();

		enableByRecord = enableByRecord.bind( this );

		if ( typeof record === 'object' ) {
			// logger.debug( '[44]record: {', record, '}' );

			enableByRecord( record );
		}
		else {
			// logger.debug( '[49]record: {', record, '}' );

			this.getRecord( record ).then( enableByRecord, dfd.reject);
		}

		return dfd.promise;

		function enableByRecord( record ) {
			// logger.debug( '[57]record: {', record, '}' );

			t_connection_query_schedule.findById(
				record.id_connection_query_schedule, {
					include: [ {
						model: t_report,

						include: [ {
							model: t_module
						} ]
					}, {
						model: t_connection,

						include: [ {
							model: t_connection_type
						} ]
					}, {
						model: t_group,

						include: [ {
							model: t_connection_type
						} ]
					} ]
				} )
			.then( function( connection_query_schedule ) {
				// logger.debug( '[84]connection_query_schedule: {', connection_query_schedule, '}' );

				if ( connection_query_schedule ) {
					var result = {};

					result.id_connection_query_schedule = connection_query_schedule.id_connection_query_schedule;
					result.connType                     = connection_query_schedule.t_connection.t_connection_type.connection_type_name;
					result.connName                     = connection_query_schedule.t_connection.serverName;
					result.moduleType                   = connection_query_schedule.t_report.t_module.module_name;
					result.repName                      = connection_query_schedule.t_report.report_name;
					result.isEnabled                    = connection_query_schedule.isEnabled;
					result.schedule                     = connection_query_schedule.schedule;

					// logger.debug( 'result.id_connection_query_schedule: {', result.id_connection_query_schedule, '}' );
					// logger.debug( 'result.connType: {', result.connType, '}' );
					// logger.debug( 'result.connName: {', result.connName, '}' );
					// logger.debug( 'result.moduleType: {', result.moduleType, '}' );
					// logger.debug( 'result.repName: {', result.repName, '}' );
					// logger.debug( 'result.isEnabled: {', result.isEnabled, '}' );
					// logger.debug( 'result.schedule: {', result.schedule, '}' );

					var repName    = result.repName;
					var connType   = result.connType;
					var connName   = result.connName;
					var moduleType = result.moduleType;
					var schedule   = result.schedule;
					var nameJob    = getCurrNameJobPtr(result);

					if ( !record.forGroup ) {
						// logger.debug( '[93]nameJob: {', nameJob, '}' );
						// logger.debug( '[94]schedule: {', schedule, '}' );

						active_schedule = nodeSchedule.scheduleJob( nameJob, schedule, function() {
							// logger.debug( '[101]repName: {', repName, '}' );
							// logger.debug( '[102]connType: {', connType, '}' );
							// logger.debug( '[103]connName: {', connName, '}' );
							// logger.debug( '[104]moduleType: {', moduleType, '}' );
							// logger.debug( '[105]recent: {', recent, '}' );

							Report( sessionId ).getReportByScheduler( repName, connType, connName, moduleType, recent )
								.then( data => {
									return dfd.resolve( data );
								} )
								.catch( err => {
									logger.error( '[112]', err );

									return dfd.reject( err );
								} )
						} );
					}
					else {
						dbsched.t_group.findOne( {
							where: {
								group_name: record.groupName
							}
						} ).then( function( rData ) {
							dbsched.t_connection_group.findAndCountAll( {
								where: {
									id_group: rData.id_group
								}
								,include: [ {
									model: dbsched.t_connection
								} ]
							} ).then( function( aData ) {
								var aProm = [];

								if ( aData.count ) {
									for ( var i = 0; i < aData.count; i++ ) {
										aProm.push( new Promise( function( resolve, reject ) {
											var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );

											active_schedule = nodeSchedule.scheduleJob( nameJob, schedule, function() {
												Report( sessionId ).getReportByScheduler( repName, connType, connName2, moduleType, recent )
													.then( function( repData ) {
														resolve( repData );
													},
													function( err ) {
														logger.error( '[132]', err );

														reject( err );
													} );
											} );
										} ) );
									}

									if ( aProm.length ) {
										return Promise.all( aProm ).then( function() {
											return dfd.resolve();
										} );
									}
								}
								else {
									return dfd.reject();
								}
							} );
						} );
					}

					updateSelectorPtr( result, true )
						.then(dfd.resolve, dfd.reject);
				}
			});
		}
	};

	/**
	 * Disable determined task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.disable = function( record, sessionId ) {
		// logger.debug( '[196]Scheduler.prototype.disable' );
		// logger.debug( '[197]record: {', record, '}' );
		// logger.debug( '[198]sessionId: {', sessionId, '}' );

		var dfd  = Q.defer();
		var jobs = this.getStartedJobs();

		disableByRecord = disableByRecord.bind( this );

		if ( typeof record === 'object' ) {
			// logger.debug( '[190]record: {', record, '}' );

			disableByRecord( record );
		}
		else {
			// logger.debug( '[195]record: {', record, '}' );

			this.getRecord( record ).then( disableByRecord, dfd.reject );
		}

		return dfd.promise;

		function disableByRecord( record ) {
			t_connection_query_schedule.findById(
				record.id_connection_query_schedule, {
					include: [ {
						model: t_report,

						include: [ {
							model: t_module
						} ]
					}, {
						model: t_connection,

						include: [ {
							model: t_connection_type
						} ]
					}, {
						model: t_group,

						include: [ {
							model: t_connection_type
						} ]
					} ]
				} )
			.then( function( connection_query_schedule ) {
				// logger.debug( '[242]connection_query_schedule: {', connection_query_schedule, '}' );

				if ( connection_query_schedule ) {
					var result = {};

					result.id_connection_query_schedule = connection_query_schedule.id_connection_query_schedule;
					result.connType                     = connection_query_schedule.t_connection.t_connection_type.connection_type_name;
					result.connName                     = connection_query_schedule.t_connection.serverName;
					result.moduleType                   = connection_query_schedule.t_report.t_module.module_name;
					result.repName                      = connection_query_schedule.t_report.report_name;
					result.isEnabled                    = connection_query_schedule.isEnabled;
					result.schedule                     = connection_query_schedule.schedule;

					// logger.debug( 'result.id_connection_query_schedule: {', result.id_connection_query_schedule, '}' );
					// logger.debug( 'result.connType: {', result.connType, '}' );
					// logger.debug( 'result.connName: {', result.connName, '}' );
					// logger.debug( 'result.moduleType: {', result.moduleType, '}' );
					// logger.debug( 'result.repName: {', result.repName, '}' );
					// logger.debug( 'result.isEnabled: {', result.isEnabled, '}' );
					// logger.debug( 'result.schedule: {', result.schedule, '}' );

					var currJobName = getCurrNameJobPtr(result);

					if ( jobs.hasOwnProperty(currJobName) ) {
						jobs[currJobName].cancel();

						updateSelectorPtr(result, false)
							.then(dfd.resolve, dfd.reject);
					}
					else {
						dfd.reject( {
							'result': 'job not found'
						} );
					}
				}
			});
		};
	};

	function getCurrNameJobPtr( record ) {
		// logger.debug( '[282]getCurrNameJobPtr' );

		//
		// TODO: refactoring is required
		// TODO: use parameter 'id_connection_query_schedule' instead of the combined string
		// TODO: suggest to remove this function
		//
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	Scheduler.prototype.getStartedJobs = function() {
		// logger.debug( '[295]Scheduler.prototype.getStartedJobs' );

		return nodeSchedule.scheduledJobs;
	};

	Scheduler.prototype.list = function() {
		// logger.debug( '[301]Scheduler.prototype.list' );

		var tables = [];
		var dfd    = Q.defer();

		t_connection_query_schedule.findAll( {
			include: [ {
				model: t_report,

				include: [ {
					model: t_module
				} ]
			}, {
				model: t_connection,

				include: [ {
					model: t_connection_type
				} ]
			}, {
				model: t_group,

				include: [ {
					model: t_connection_type
				} ]
			} ]
		} ).then( function( table ) {
			var result = table.map( extractItem );

			if ( result ) {
				tables.push( result );
			}

			dfd.resolve(tables);
		} )
		.finally(function() {
			dfd.resolve( tables );
		} );

		return dfd.promise;

		function extractItem( item ) {
			var dataValues = item.dataValues;
			var resItem    = {};

			for ( var key in dataValues ) {
				if ( dataValues.hasOwnProperty(key) ) {
					resItem[key] = dataValues[key];
				}
			}

			resItem.repName    = dataValues.t_report.report_name;
			resItem.moduleType = dataValues.t_report.t_module.module_name;
			resItem.connType   = dataValues.t_connection.t_connection_type.connection_type_name;
			resItem.connName   = dataValues.t_connection.serverName;

			return resItem;
		};
	};

	/**
	 * insert (create) new scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.insert = function( record, sessionId ) {
		// logger.debug( '[361]Scheduler.prototype.insert' );
		// logger.debug( '[362]record: {', record, '}' );
		// logger.debug( '[363]sessionId: {', sessionId, '}' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.sync().then( function() {
			var promises = [];

			promises.push( new Promise( function( resolve, reject ) {
				// logger.debug( 'record.connType: {', record.connType, '}' );

				dbsched.t_connection_type.findOrCreate( {
					where: {
						connection_type_name: record.connType
					}
				}).spread( function( TConnectionType, created ) {
					// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

					// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
					// logger.debug( 'record.connName: {', record.connName, '}' );

					dbsched.t_connection.findOrCreate( {
						where: {
							id_connection_type: TConnectionType.id_connection_type,
							serverName:         record.connName
						}
					}).spread( function( TConnection, created ) {
						// logger.debug( 'TConnection.id_connection: {', TConnection.id_connection, '}' );

						// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
						// logger.debug( 'record.groupName: {', record.groupName, '}' );

						dbsched.t_group.findOrCreate( {
							where: {
								id_connection_type: TConnectionType.id_connection_type,
								group_name:         record.groupName
							}
						}).spread( function( TGroup, created ) {
							// logger.debug( 'TGroup.id_group: {', TGroup.id_group, '}' );

							// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
							// logger.debug( 'record.moduleType: {', record.moduleType, '}' );

							dbsched.t_module.findOrCreate( {
								where: {
									id_connection_type: TConnectionType.id_connection_type,
									module_name:        record.moduleType
								}
							} ).spread( function( TModule, created ) {
								// logger.debug( 'TModule.id_module: {', TModule.id_module, '}' );

								// logger.debug( 'TModule.id_module: {', TModule.id_module, '}' );
								// logger.debug( 'record.repName: {', record.repName, '}' );

								dbsched.t_report.findOrCreate( {
									where: {
										id_module:   TModule.id_module,
										report_name: record.repName
									}
								}).spread( function( TReport, created ) {
									// logger.debug( 'report.id_report: {', TReport.id_report, '}' );

									// logger.debug( 'TConnection.id_connection: {', TConnection.id_connection, '}' );
									// logger.debug( 'TGroup.id_group: {', TGroup.id_group, '}' );
									// logger.debug( 'record.forGroup: {', record.forGroup, '}' );
									// logger.debug( 'TReport.id_report: {', TReport.id_report, '}' );
									// logger.debug( 'record.schedule: {', record.schedule, '}' );
									// logger.debug( 'record.isEnabled: {', record.isEnabled, '}' );

									dbsched.t_connection_query_schedule.create( {
										'id_connection': TConnection.id_connection,
										'id_group':      TGroup.id_group,
										'forGroup':      record.forGroup,
										'id_report':     TReport.id_report,
										'schedule':      record.schedule,
										'isEnabled':     record.isEnabled
									} ).then( function( res ) {
										// logger.debug( 'res.id_connection_query_schedule: {', res.id_connection_query_schedule, '}' );

										record.id_connection_query_schedule = res.id_connection_query_schedule;

										resolve( record );
									}).catch(function( err ) {
										logger.error( '[376]', err );

										reject( err );
									});
								});
							});
						});
					});
				});
			}));

			Promise.all( promises ).then( function( resp ) {
				dfd.resolve(resp);
			}, function( err ) {
				logger.error( '[390]', err );

				dfd.reject(err);
			});
		});

		return dfd.promise;
	};

	/**
	 * update existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.update = function( id_connection_query_schedule, newRecord, sessionId ) {
		// logger.debug( '[481]Scheduler.prototype.update' );
		// logger.debug( '[482]id_connection_query_schedule: {', id_connection_query_schedule, '}' );
		// logger.debug( '[483]newRecord: {', newRecord, '}' );
		// logger.debug( '[484]sessionId: {', sessionId, '}' );

		var enablePtr = this.enable;

		// set 'id_connection_query_schedule'
		newRecord.id_connection_query_schedule = id_connection_query_schedule;

		return this.getRecord( id_connection_query_schedule ).then( function( record ) {
			// logger.debug( 'record: {', record, '}' );

			this.disable( record, sessionId )
				.then( function() {
					// logger.debug( '[421]newRecord.connType: {', newRecord.connType, '}' );

					return dbsched.t_connection_type.findOrCreate( {
						where: {
							connection_type_name: newRecord.connType
						}
					} ).spread( function( TConnectionType, created ) {
						// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

						// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );
						// logger.debug( 'newRecord.connName: {', newRecord.connName, '}' );

						dbsched.t_connection.findOrCreate( {
							where: {
								id_connection_type: TConnectionType.id_connection_type,
								serverName:         newRecord.connName
							}
						} ).spread( function( TConnection, created ) {
							// logger.debug( 'TConnection.id_connection: {', TConnection.id_connection, '}' );

							// set 'id_connection'
							newRecord.id_connection = TConnection.id_connection;

							// logger.debug( 'newRecord.moduleType: {', newRecord.moduleType, '}' );
							// logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

							dbsched.t_module.findOrCreate( {
								where: {
									module_name:        newRecord.moduleType,
									id_connection_type: TConnectionType.id_connection_type
								}
							} ).spread( function( TModule, created ) {
								// logger.debug( 'TModule.id_module: {', TModule.id_module, '}' );

								// logger.debug( 'TModule.id_module: {', TModule.id_module, '}' );
								// logger.debug( 'newRecord.repName: {', newRecord.repName, '}' );

								dbsched.t_report.findOrCreate( {
									where: {
										id_module:   TModule.id_module,
										report_name: newRecord.repName
									}
								}).spread( function( TReport, created ) {
									// logger.debug( 'TReport.id_report: {', TReport.id_report, '}' );

									// set 'id_report'
									newRecord.id_report = TReport.id_report;

									record.updateAttributes( newRecord )
										.then( function( result ) {
											enablePtr( newRecord, sessionId );
										}.bind( this ));
								} );
							} );
						} );
					} );
				}.bind(this), function() {
					// logger.debug( '[475]newRecord: {', newRecord, '}' );

					record.updateAttributes( newRecord );
				} );
		}.bind( this ));
	};

	/**
	 * delete (remove) existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	Scheduler.prototype.delete = function( id_connection_query_schedule, sessionId ) {
		// logger.debug( '[567]Scheduler.prototype.delete' );

		var dfd = Q.defer();

		// logger.debug( 'Scheduler.prototype.delete' );

		this.getRecord( id_connection_query_schedule ).then(function( record ) {
			this.disable( record, sessionId ).finally( function() {
				dbsched.t_connection_query_schedule.sync().then( function() {
					dbsched.t_connection_query_schedule.destroy( {
						where: {
							id_connection_query_schedule: id_connection_query_schedule
						}
					} ).then(dfd.resolve, dfd.reject);
				}, dfd.reject);
			});
		}.bind(this), dfd.reject);

		return dfd.promise;
	};

	/**
	 * update 'enabled' attribute for the existing scheduled task
	 *
	 * @param {mixed} record It may be record object OR recordID
	 * @returns {Q@call;defer.promise}
	 */
	function updateSelectorPtr( record, selector ) {
		// logger.debug( '[595]updateSelectorPtr' );
		// logger.debug( '[596]record: {', record, '}' );
		// logger.debug( '[597]selector: {', selector, '}' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.sync().then( function() {
			var promises = [];

			promises.push(function( next ) {
				dbsched.t_connection_query_schedule.find( {
					where: {
						id_connection_query_schedule: record.id_connection_query_schedule
					}
				} )
				.then( function ( record ) {
					if ( record ) {
						record.updateAttributes( {
							isEnabled: selector
						} ).catch( function( err ) {
							logger.error( '[538]', err );

							dfd.reject(err);
						} );
					}
				}).finally(next);;
			});

			async.auto(promises, function( err, results ) {
				if ( err ) {
					logger.error( '[548]', err );

					dfd.reject( err );
				}
				else {
					dfd.resolve( results );
				}
			});
		});

		return dfd.promise;
	};

	Scheduler.prototype.getRecord = function( id_connection_query_schedule ) {
		// logger.debug( '[639]Scheduler.prototype.getRecord' );

		var dfd = Q.defer();

		dbsched.t_connection_query_schedule.findById( id_connection_query_schedule ).then( function ( record ) {
			if ( record ) {
				dfd.resolve( record );
			}
			else {
				logger.error( '[585]record not found:id_connection_query_schedule: {', id_connection_query_schedule, '}' );

				dfd.reject( 'record not found' );
			}
		}, dfd.reject);

		return dfd.promise;
	};

	/*
	 * a storage for all schedulers
	 */
	var scheduler_instances = { };

	/*
	 * a factory for report instances
	 */
	module.exports = function( sessionId ) {
		// logger.debug( '[666]module.exports' );

		if ( !( sessionId in scheduler_instances ) ) {
			scheduler_instances[sessionId] = new Scheduler();
		}

		return scheduler_instances[sessionId];
	};
} )( module );
