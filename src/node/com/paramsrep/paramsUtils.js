'use strict';

(function() {
	var Q               = require('q');
	var path            = require('path');
	var Promise         = require('promise');
	var fs              = require('fs');
	var dbparams        = require('src/node/database/model').t_query_parameter;
	var Filters         = require('src/node/com/report/filters');
	var constTypeNumber = 'Number'; // Type for cast

	/*
	 * Function, that get file name with version
	 * of DB server
	 *
	 * @param {array} file
	 * @param {string} version
	 * @return {string}
	 */
	module.exports.getFileNameFromVersion = function( file, version ) {
		var ret                          = '';
		var strMinVersion                = '';
		var strMaxVersion                = '';
		var arrayCurrentVersionComponent = version.split('.');
		var arrayMinVersionComponent     = [];
		var arrayMaxVersionComponent     = [];
		var boolResult                   = false;

		// console.log('param file:', file);
		// console.log('version:', version);

		for ( var intFileIndex = 0; intFileIndex < file.length; intFileIndex++ ) {
			// extract minimum version components
			if ( !( 'min' in file[intFileIndex] )) {
				strMinVersion = '*';
			}
			else {
				strMinVersion = file[intFileIndex].min;
			}

			// extract maximum version components
			if ( !( 'max' in file[intFileIndex] )) {
				strMaxVersion = '*';
			}
			else {
				strMaxVersion = file[intFileIndex].max;
			}

			// console.log('strMinVersion:', strMinVersion);
			// console.log('strMaxVersion:', strMaxVersion);

			for ( var intVersionComponentIndex = 0; intVersionComponentIndex < arrayCurrentVersionComponent.length; intVersionComponentIndex++ ) {
				// console.log('arrayCurrentVersionComponent[intVersionComponentIndex]:', arrayCurrentVersionComponent[intVersionComponentIndex]);

				// check 'min' component
				arrayMinVersionComponent = strMinVersion.split('.');

				if (intVersionComponentIndex < arrayMinVersionComponent.length) {
					if (arrayMinVersionComponent[intVersionComponentIndex] != '*') {
						// console.log('arrayMinVersionComponent[intVersionComponentIndex]:', arrayMinVersionComponent[intVersionComponentIndex]);

						if (parseInt(arrayCurrentVersionComponent[intVersionComponentIndex], 10) >= parseInt(arrayMinVersionComponent[intVersionComponentIndex], 10)) {
							boolResult = true;
						}
						else {
							boolResult = false;
						}
					}
					else {
						boolResult = true;
					}
				}
				else {
					boolResult = true;
				}

				if (boolResult === true) {
					// check 'max' component
					arrayMaxVersionComponent = strMaxVersion.split('.');

					if (intVersionComponentIndex < arrayMaxVersionComponent.length) {
						if (arrayMaxVersionComponent[intVersionComponentIndex] != '*') {
							// console.log('arrayMaxVersionComponent[intVersionComponentIndex]:', arrayMaxVersionComponent[intVersionComponentIndex]);

							if (parseInt(arrayCurrentVersionComponent[intVersionComponentIndex], 10) <= parseInt(arrayMaxVersionComponent[intVersionComponentIndex], 10)) {
								boolResult = true;
							}
							else {
								boolResult = false;
							}
						}
						else {
							boolResult = true;
						}
					}
					else {
						boolResult = true;
					}
				}

				if (boolResult === false) {
					break;
				}
			}

			if (boolResult === true) {
				ret = file[intFileIndex].file;
				break;
			}
		}

		// console.log('ret file:', ret);
		return ret;
	};


	/*
	 * Function, that get parameters from
	 * each paragraph and each sections
	 *
	 * @param {object} report
	 * @return {object}
	 */
	module.exports.getExtractParameters = function( report, typeConnect, serverName, nameRep, connector, pathSql ) {
		var aExtract     = [];
		var aFile        = [];
		var aPromExtract = [];
		var request      = report.get( 'request' );
		var posExt       = 0;
		var posTr        = 0;
		var prefs        = {};

		if ( request ) {
			request.forEach( function( it_extract ) {
				var params = it_extract.parameters;

				if ( params ) {
					aExtract.push( { sectionName: it_extract.sectionName, params: [] } );
					posExt = aExtract.length - 1;

					params.forEach( function( param ) {
						aPromExtract.push( new Promise( function( resolve, reject ) {
							param.connType      = typeConnect;
							param.connName      = serverName;
							param.nameRep       = nameRep;
							param.paragraphName = 'extract';
							param.sectionName   = aExtract[posExt].sectionName;

							dbparams.find( {
								where: {
									// connType:      param.connType,
									// connName:      param.connName,
									// repName:       param.nameRep,
									// TODO SSK check report name, ...
									nameParam:     param.name,
									paragraphName: param.paragraphName,
									sectionName:   param.sectionName
								}
							} )
							.then( function( record ) {
								var fileName = '';
								var request  = null;

								// if ( record ) {
								// 	switch( param.show ) {
								// 		case constTypeNumber:
								// 			param.value = parseInt(record.valueParam);
								// 			break;
								// 		default:
								// 			param.value = record.valueParam;
								// 			break;
								// 	}
								// }

								if ( param.query.hasOwnProperty( 'file' ) ) {
									param.query.valquery = [];

									fileName = param.query.file;

									try {
										request = fs.readFileSync( path.join(pathFile, fileName), 'utf8' );
									}
									catch ( err ) {
										resolve( param );
										return;
									}

									connector.query( request ).then( function( records ) {
										records = Filters.delArrayDataSets( records );
										param.query.valquery.push( records );
										resolve( param );
									}, function( err ) {
										reject( err );
									});
								}
								else {
									resolve( param );
								}

							} );
						} ) );
					} );
				}
			} );

			if ( aPromExtract.length > 0 ) {
				return Promise.all( aPromExtract ).then( function( ext_resolved ) {
					var pos = 0;

					if ( ext_resolved ) {
						for ( var ind = 0; ind < ext_resolved.length; ind++ ) {
							pos = findInArrayObjects( aExtract, 'sectionName', ext_resolved[ind].sectionName );

							if ( pos == -1 ) {
								aExtract.push( { sectionName: ext_resolved[ind].sectionName, params: [] } );
								pos = aExtract.length - 1;
							}

							aExtract[pos].params.push( ext_resolved[ind] );
						}
					}

					return aExtract;
				} );
			}
			else {
				//dfd.reject( {error: 'Not find property parameters'} );
				//return dfd.promise;
				aPromExtract.push( new Promise( function( resolve, reject ) {
					resolve( {} );
				} ) );
				return Promise.all( aPromExtract ).then( function() {
					aExtract.push( { sectionName: '', params: [] } );

					return aExtract;
				} );
			}
		}
		else {
			//dfd.reject( {error: 'Not find property parameters'} );
			//return dfd.promise;
			aPromExtract.push( new Promise( function( resolve, reject ) {
				resolve( {} );
			} ) );

			return Promise.all( aPromExtract ).then( function() {
				aExtract.push( { sectionName: '', params: [] } );

				return aExtract;
			} );
		}
	};

	/*
	 * Function, that get parameters from
	 * paragraph transformation and each sections
	 *
	 * @param {object} report
	 * @return {object}
	 */
	module.exports.getTransformParameters = function( report, typeConnect, serverName, nameRep, connector, pathSql ) {
		var aTransform   = [];
		var aPromExtract = [];
		var request      = report.get( 'request_trans' );
		var posExt       = 0;
		var posTr        = 0;
		var prefs        = {};

		if ( request ) {
			request.forEach( function( it_transform ) {
				var params = it_transform.parameters;

				if ( params ) {
					aTransform.push( { sectionName: it_transform.sectionName, params: [] } );

					posExt = aTransform.length - 1;

					params.forEach( function( param ) {
						aPromExtract.push( new Promise( function( resolve, reject ) {
							param.connType      = typeConnect;
							param.connName      = serverName;
							param.nameRep       = nameRep;
							param.paragraphName = 'transformation';
							param.sectionName   = aTransform[posExt].sectionName;

							dbparams.find( {
								where: {
									// connType:      param.connType,
									// connName:      param.connName,
									// repName:       param.nameRep,
									// TODO check report name, ...
									nameParam:     param.name,
									paragraphName: param.paragraphName,
									sectionName:   param.sectionName
								}
							} )
							.then( function( record ) {
								var fileName = '';
								var request  = null;

								// if ( record ) {
								// 	switch( param.show ) {
								// 		case constTypeNumber:
								// 			param.value = parseInt( record.valueParam );
								// 			break;
								// 		default:
								// 			param.value = record.valueParam;
								// 			break;
								// 	}
								// }

								if ( param.query.hasOwnProperty( 'file' ) ) {
									param.query.valquery = [];

									fileName = param.query.file;

									try {
										request = fs.readFileSync( path.join(pathFile, fileName), 'utf8' );
									}
									catch ( err ) {
										resolve( param );
										return;
									}

									connector.query( request ).then( function( records ) {
										records = Filters.delArrayDataSets( records );
										param.query.valquery.push( records );
										resolve( param );
									}, function( err ) {
										reject( err );
									});
								}
								else {
									resolve( param );
								}
							} );
						} ) );
					} );
				}
			} );

			if ( aPromExtract.length > 0 ) {
				return Promise.all( aPromExtract ).then( function( ext_resolved ) {
					var pos = 0;

					if ( ext_resolved ) {
						for ( var ind = 0; ind < ext_resolved.length; ind++ ) {
							pos = findInArrayObjects( aTransform, 'sectionName', ext_resolved[ind].sectionName );

							if ( pos == -1 ) {
								aTransform.push( { sectionName: ext_resolved[ind].sectionName, params: [] } );
								pos = aTransform.length - 1;
							}
							aTransform[pos].params.push( ext_resolved[ind] );
						}
					}
					return aTransform;
				} );
			}
			else {
				//dfd.reject( {error: 'Not find property parameters'} );
				//return dfd.promise;
				aPromExtract.push( new Promise( function( resolve, reject ) {
					resolve( {} );
				} ) );

				return Promise.all( aPromExtract ).then( function() {
					aTransform.push( { sectionName: '', params: [] } );
					return aTransform;
				} );
			}
		}
		else {
			//dfd.reject( {error: 'Not find property parameters'} );
			aPromExtract.push( new Promise( function( resolve, reject ) {
				resolve( {} );
			} ) );

			return Promise.all( aPromExtract ).then( function() {
				aTransform.push( { sectionName: '', params: [] } );
				return aTransform;
			} );
		}
	};

	/*
	 * Function, that get parameters from
	 * paragraph load
	 *
	 * @param {object} report
	 * @return {object}
	 */
	module.exports.getLoadParameters = function( report, typeConnect, serverName, nameRep, connector, pathSql ) {
		var aLoad        = [];
		var params       = report.get( 'parameters' );
		var aLoadExtract = [];
		var posExt       = 0;

		if ( params ) {
			aLoad.push( { sectionName: 'load', params: [] } );

			params.forEach( function( param ) {
				aLoadExtract.push( new Promise( function( resolve, reject ) {
					param.connType      = typeConnect;
					param.connName      = serverName;
					param.nameRep       = nameRep;
					param.paragraphName = 'load';
					param.sectionName   = aLoad[posExt].sectionName;

					dbparams.find( {
						where: {
							// connType:      param.connType,
							// connName:      param.connName,
							// repName:       param.nameRep,
							// TODO SSK check report name, ...
							nameParam:     param.name,
							paragraphName: param.paragraphName,
							sectionName:   param.sectionName
						}
					} )
					.then( function( record ) {
						var fileName = '';
						var request  = null;

						// if ( record ) {
						// 	switch( param.show ) {
						// 		case constTypeNumber:
						// 			param.value = parseInt( record.valueParam );
						// 			break;
						// 		default:
						// 			param.value = record.valueParam;
						// 			break;
						// 	}
						// }

						if ( param.query.hasOwnProperty( 'file' ) ) {
							param.query.valquery = [];

							fileName = param.query.file;

							try {
								request = fs.readFileSync( path.join( pathFile, fileName ), 'utf8' );
							}
							catch( err ) {
								resolve( param );

								return;
							}

							connector.query( request ).then( function( records ) {
								records = Filters.delArrayDataSets( records );
								param.query.valquery.push( records );
								resolve( param );
							}, function( err ) {
								reject( err );
							});
						}
						else {
							resolve( param );
						}

					} );
				} ) );
			} );

			if ( aLoadExtract.length > 0 ) {
				return Promise.all( aLoadExtract ).then( function( load_resolved ) {
					aLoad[0].params = load_resolved;
					return aLoad;
				} );
			}
			else {
				aLoadExtract.push( new Promise( function( resolve, reject ) {
					resolve( {} );
				} ) );

				return Promise.all( aLoadExtract ).then( function() {
					return aLoad;
				} );
			}
		}
		else {
			aLoadExtract.push( new Promise( function( resolve, reject ) {
				resolve( {} );
			} ) );

			return Promise.all( aLoadExtract ).then( function() {
				return aLoad;
			} );
		}
	};

	/*
	 * function search value
	 * in array of objects
	 */
	function findInArrayObjects( arr, key, value ) {
		var ret = -1;

		if ( !arr || !key ) {
			return ret;
		}

		for ( var i = 0; i < arr.length; i++ ) {
			if ( key in arr[i] ) {
				if ( arr[i][key] == value ) {
					ret = i;
					break;
				}
			}
		}

		return ret;
	}
})();
