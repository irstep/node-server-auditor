'use strict';

(function (module) {
	var Sequelize = require('sequelize');
	var extend    = require('extend');

	module.exports = function ( database, name, schema, primaryKeys ) {
		var newSchema = {};

		extend(newSchema, schema);

		newSchema._id_connection = {
			type: Sequelize.INTEGER
		};

		if ( primaryKeys ) {
			newSchema._int_order = Sequelize.INTEGER;

			extend( newSchema._id_connection, {
				primaryKey: true
			} );

			newSchema['_hash'] = {
				type: Sequelize.CHAR(32)
			};

			primaryKeys.forEach(function (keyName) {
				if ( keyName in newSchema ) {
					newSchema[keyName] = {
						type:       newSchema[keyName],
						primaryKey: true
					};
				}
			});
		}

		var model = database.define(name, newSchema);

		return model;
	};
})(module);
