'use strict';

(function (module) {
	var Q        = require('q');
	var DB_Model = require('src/node/database/model');

	module.exports = function (serverName, needSync) {
		return (needSync ? DB_Model.t_connection.sync() : Q(null)).then( function() {
			var dfd = Q.defer();

			DB_Model.t_connection.findCreateFind( {
				where: {
					serverName: serverName
				},
				defaults: {
					serverName: serverName
				}
			})
				.spread(dfd.resolve).catch(dfd.reject);

			return dfd.promise;
		});
	};
})(module);
