// 'use strict';

(function (module, Object) {
	var Q           = require('q');
	var extend      = require('extend');
	var fs          = require('fs');
	var requireDir  = require('require-dir');
	var path        = require('path');
	var Data        = require('src/node/com/data');
	var initConfig  = require('src/node/database/initial');
	var db          = require('src/node/database')();
	var logger      = require('src/node/log');
	var ParamsUtils = require('src/node/com/paramsrep/paramsUtils');
	var appDir      = path.dirname(require.main.filename);
	var modulesPath = path.join(appDir, 'data');

	var ReportModuleFactory = function (name, serverVersion) {
		// logger.debug('reportModuleFactory.js:ReportModuleFactory:name: ', name);

		ReportModuleFactory.getAvailable().then( function( available ) {
			if (available.indexOf(name) === -1) {
				logger.error( '[22]no report modules defined' );
				logger.error( '[23]reportModuleFactory.js:ReportModuleFactory:name: {', name, '}' );
				logger.error( '[24]reportModuleFactory.js:ReportModuleFactory:serverVersion: {', serverVersion, '}' );

				throw 'no report modules defined';
			}
		} );

		this.name = name;

		var json = require(path.join(modulesPath, this.name));

		return new Data.ReportModule({
			name:        json.name,
			type:        json.type,
			description: json.description,
			reports:     mapReports(json.reports, serverVersion),
			repJson:     jsonMenu(json.reports, serverVersion)
		});

		function jsonMenu(reports, serverVersion) {
			var report     = null;
			var collection = new Data.ReportCollection();
			var rows       = [];
			var currentRow = null;
			var fileNames  = [];

			for ( var key in reports ) {
				currentRow = {
					reports: [],
					section: key
				};

				reports[key].forEach(function( reportPath ) {
					var fileName = '';

					report = require(path.join(modulesPath, reportPath));

					fileNames = [];

					report.extract.forEach( function( it_extract ) {
						fileName = ParamsUtils.getFileNameFromVersion( it_extract.file, serverVersion );

						// logger.debug( '[65]reportModuleFactory.js:jsonMenu:fileName: {', fileName, '}' );

						fileNames.push( fileName );
					} );

					currentRow.reports.push( {
						'name':    report.name,
						'pathSql': reportPath,
						'file':    fileNames
					} );
				});

				rows.push(currentRow);
			}

			return rows;
		}

		function mapReports(reports, serverVersion) {
			var collection         = new Data.ReportCollection();
			var arr_request        = [];
			var arr_transformation = [];

			for ( var key in reports ) {
				reports[key].forEach( function( sectionRep ) {
					var report    = require( path.join(modulesPath, sectionRep) );
					var reportDir = path.dirname( path.join( modulesPath, sectionRep ));
					var fileName  = '';

					arr_request        = [];
					report.pathSql     = sectionRep;
					arr_transformation = [];

					if ( 'extract' in report ) {
						report.extract.forEach( function( it_extract ) {
							fileName = ParamsUtils.getFileNameFromVersion( it_extract.file, serverVersion );

							// logger.debug( '[102]reportModuleFactory.js:mapReports:extract:reportDir: {', reportDir, '}' );
							// logger.debug( '[103]reportModuleFactory.js:mapReports:extract:FileName: {', fileName, '}' );
							// logger.debug( '[104]reportModuleFactory.js:mapReports:extract:FullFileName: {', path.join( reportDir, fileName), '}' );

							if ( fileName != null && fileName != '' ) {
								arr_request.push( {
									report:      it_extract,
									order:       it_extract.order || 0,
									query:       fs.readFileSync(path.join( reportDir, fileName), 'utf8'),
									parameters:  it_extract.parameters,
									sectionName: it_extract.sectionName || ''
								} );
							}
						});
					}

					if ( 'transformation' in report ) {
						report.transformation.forEach( function( it_transformation ) {
							fileName = it_transformation.file;

							// logger.debug( '[117]reportModuleFactory.js:mapReports:transformation:FileName: {', fileName, '}' );
							// logger.debug( '[118]reportModuleFactory.js:mapReports:transformation:FullFileName: {', path.join( reportDir, fileName), '}' );

							arr_transformation.push( {
								query:       fs.readFileSync(path.join( reportDir, fileName), 'utf8'),
								parameters:  it_transformation.parameters,
								storage:     it_transformation.storage,
								order:       it_transformation.order,
								sectionName: it_transformation.sectionName || ''
							} );
						});
					}

					if ( 'load' in report ) {
						report.show_query_file = report.load.file;
						report.storage         = report.load.storage;
						report.parameters      = report.load.parameters;
					}

					collection.add(
						new Data.ReportItem(
							report,
							arr_request,
							report.show_query_file ? fs.readFileSync( path.join( reportDir, report.show_query_file), 'utf8') : null,
							arr_transformation
						)
					);
				});
			}

			return collection;
		}
	};

	ReportModuleFactory.getAvailable = function() {
		var reports = [];

		return db.models.t_module.findAll( {
			where: {
				is_active: true
			}
		}).then( function( data ) {
			data.forEach( function(val, idx) {
				reports.push( val.module_name );
			})

			return Q.resolve( reports );
		})
	};

	ReportModuleFactory.getAvailableExtended = function() {
		var kvp    = {};
		var result = [];

		return db.models.t_report.findAll( {
			where: {
				is_active: true
			},
			include: [ {
					model: db.models.t_module,
					include: [ {
						model: db.models.t_connection_type
					} ]
				},
			]
		} ).then( function( data ) {
			data.forEach( function( val, idx ) {
				if ( kvp.hasOwnProperty( val.t_module.module_name ) ) {
					kvp[val.t_module.module_name].reports.push( val.report_name );
				}
				else {
					kvp[ val.t_module.module_name ] = {
						name:    val.t_module.module_name,
						type:    val.t_module.t_connection_type.connection_type_name,
						reports: [ val.report_name ],
					}
				}
			} );

			for ( var p in kvp ) {
				result.push( kvp[p] );
			}

			return Q.resolve( result );
		} )
		.then( function( reportData ) {
			// get data from server groups
			return db.models.t_connection_group.findAll( {
				where: {
					is_active: true
				}
				,include: [ {
						model: db.models.t_connection
					}, {
						model: db.models.t_group
					}
				]
			} ).then( function( data ) {
				return Q.resolve( { modules: reportData, servers: data } );
			} );
		} );
	};

	module.exports = ReportModuleFactory;
})(module, Object);
