'use strict';

(function( module ) {
	var requireDir = require( 'require-dir' );
	var path       = require( 'path' );
	var appDir     = path.dirname( require.main.filename );
	var serversDir = path.join( appDir, 'servers' );

	/**
	 * Getting all avaliable servers from app with settings
	 *
	 * @returns {object Array}
	 */
	module.exports = function( name ) {
		var servers = requireDir( serversDir );
		var ret     = servers;

		if ( name ) {
			if ( name in servers ) {
				ret = servers[name];
			}
			else {
				ret = null;
			}
		}

		return ret;
	};
} )( module );
