'use strict';

(function( module ) {
	var fs        = require('fs');
	var path      = require('path');
	var extend    = require('extend');
	var logger    = require('src/node/log');
	var Connector = require('src/node/com/report/connector/');
	var appDir    = path.dirname(require.main.filename);
	var appConfig = require( path.join( appDir, 'configuration.json' ) );

	/**
	 * Get version of SQL server
	 * Use report getver
	 *
	 * @param {object} Server configuration
	 * @param {string} Module name
	 *
	 * @callback {function}
	 */
	module.exports = function( server, module, callback ) {
		// logger.debug( '[22]getVersion.js:module.exports' );
		// logger.debug( '[23]server: {', server, '}' );
		// logger.debug( '[24]module: {', module, '}' );

		var connector  = null;
		var prefs      = {};
		var verRequest = '';
		var verFile    = '';
		var type       = null;
		var posType    = -1;
		var iType      = -1;

		//
		// TODO: very bad code, need to be rewritten
		// TODO: module name could not has the connection name, like 'mssql' or 'mysql'
		// TODO: connection name should be taken from the configuration property
		//
		module = module.split( '.' );

		// logger.debug( '[41]module: {', module, '}' );

		posType = module.indexOf( 'mysql' );

		// logger.debug( '[45]posType: {', posType, '}' );

		if ( posType == -1 ) {
			posType = module.indexOf( 'mssql' );
		}

		type = module[posType];

		// logger.debug( '[53]type: {', type, '}' );

		iType = (appConfig.initials.supported[type].order)? appConfig.initials.supported[type].order : -1;

		extend( true, prefs, server.get( 'prefs' ) );

		// get from default config
		if ( iType == -1 || !( 'versionFile' in appConfig.initials.supported[type] ) ) {
			// logger.debug( '[63]iType: {', iType, '}' );

			return false;
		}

		verFile = appConfig.initials.supported[type].versionFile;

		// logger.debug( '[70]verFile: {', verFile, '}' );

		verRequest = fs.readFileSync( path.join( appDir, verFile ), 'utf8' );

		// logger.debug( '[74]verRequest: {', verRequest, '}' );

		if ( verRequest === undefined || verRequest == null ) {
			return false;
		}

		connector = new Connector[type]( prefs );

		var cn = connector.query( verRequest ).then( function( resp ) {
			var ret = '';
			var reg = null;

			if ( resp !== undefined ) {
				if ( type == 'mysql' ) {
					ret = resp[0].ServerVersion;
				}
				else if ( type == 'mssql' ) {
					ret = resp[0][0].ServerVersion;
				}

				if( iType != -1 && 'versionFile' in appConfig.initials.supported[type] ) {
					if( 'versionRegex' in appConfig.initials.supported[type] ) {
						reg = new RegExp( appConfig.initials.supported[type].versionRegex );
						ret = ret.match( reg );

						if ( typeof( ret ) == 'object' ) {
							ret = ret[0];
						}
						else {
							ret = '0';
						}
					}
				}
			}

			callback( {
				success: true,
				ver:     ret
			} );
		},
		function( respErr ) {
			logger.error( '[115]respErr: {', respErr, '}' );

			callback( {
				success: false,
				ver:     ''
			} );
		} );
	};
} )( module );
