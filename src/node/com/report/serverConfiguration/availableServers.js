'use strict';

(function (module) {
	var requireDir = require('require-dir');
	var path       = require('path');
	var appDir     = path.dirname( require.main.filename );
	var serversDir = path.join( appDir, 'servers' );

	/**
	 * Getting all avaliable servers from app
	 *
	 * @returns {object Array}
	 */
	module.exports = function() {
		var servers = requireDir( serversDir );

		return Object.keys(servers);
	};

})(module);
