'use strict';

(function (module) {
	var path       = require( 'path' );
	var validator  = require( 'validator' );
	var logger     = require( 'src/node/log' );
	var Data       = require( 'src/node/com/data' );
	var appDir     = path.dirname( require.main.filename );
	var serversDir = path.join( appDir, 'servers' );

	/**
	 * Getting configuration for the chosen server (connection)
	 *
	 * @param {string} name of the server (connection)
	 * @param {string} group of server (connection)
	 *
	 * @returns {object Object}
	 */
	module.exports = function ( name, group ) {
		var config  = null;
		var prefs   = null;
		var version = null;

		// logger.debug( '[24]name: {', name, '}' );
		// logger.debug( '[25]group: {', group, '}' );
		// logger.debug( '[26]appDir: {', appDir, '}' );
		// logger.debug( '[27]serversDir: {', serversDir, '}' );

		try {
			prefs = require(path.join( serversDir, validator.whitelist( name, 'A-Za-z0-9_.-' )));

			// logger.debug( '[30]prefs: ', prefs );

			prefs = prefs.connection;

			config = new Data.ServerConfiguration( name, prefs, version, group );
		}
		catch ( err ) {
			logger.error( '[39]getConfig.js:err: ', err );
			logger.error( '[40]getConfig.js:name: {', name, '}' );
			logger.error( '[41]getConfig.js:group: {', group, '}' );

			return;
		}

		return config;
	};
} )( module );
