'use strict';

(function(module) {
	var ServerConfiguration = {
		getAvailable:     require( './availableServers' ),
		getConfiguration: require( './getConfig' ),
		getServerVersion: require( './getVersion' ),
		getServersData:   require( './getServersData' )
	};

	module.exports = ServerConfiguration;
})(module);
