'use strict';

(function (module) {
	var Q             = require( 'q' );
	var util          = require( 'util' );
	var sql           = require( 'mysql' );
	var extend        = require( 'extend' );
	var logger        = require( 'src/node/log' );
	var ConnectorType = require( './' );

	var MySQL = function() {
		MySQL.super_.apply( this, arguments );
	};

	util.inherits(MySQL, ConnectorType);

	MySQL.prototype.query = function( query ) {
		logger.debug( '[18]MySQL.prototype.query' );
		logger.debug( '[19]query: {', query, '}' );
		logger.debug( '[20]this.config: {', this.config, '}' );

		var dfd        = Q.defer();
		var connection = null;

		connection = sql.createConnection( this.config );

		logger.debug( '[27]connection: {', connection, '}' );

		connection.query( query, function( err, rows ) {
			if ( err ) {
				logger.error( '[31]', err );
				logger.error( '[32]mysql driver error:query' );
				// logger.debug(query);

				dfd.reject(err);
			}
			else {
				logger.debug( '[38]resolved' );

				dfd.resolve( rows );
			}
		} );

		connection.end();

		return dfd.promise;
	};

	module.exports = MySQL;
} )( module );
