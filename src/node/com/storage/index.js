'use strict';

(function (module) {
	var Storage = {};

	var handler = function (key, value) {
		if ( value !== undefined ) {
			return Storage[key] = value;
		}
		else {
			return Storage[key];
		}
	};

	module.exports = handler;
})(module);
