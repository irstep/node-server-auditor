'use strict';

( function(module) {
	var Q                 = require('q');
	var _                 = require('lodash');
	var requireDir        = require('require-dir');
	var path              = require('path');
	var Promise           = require('promise');
	var initials          = require('configuration').initials;
	var db                = require('src/node/database')();
	var promiseWhile      = require('src/node/com/report/snapShot/promiseWhile');
	var logger            = require('src/node/log');
	var sqliteInitScripts = require('./sql_initializations')
	var appDir            = path.dirname(require.main.filename);
	var modulesPath       = path.join(appDir, 'data');
	var newconfig         = {};
	var reverseSupported  = {};
	var serverConfig      = require('src/node/com/report/serverConfiguration');
	var date              = new Date();

	function init() {
		logger.debug( '[22]initial.js:init' );

		return db.sync()
			.then( sqliteInitScripts )
			.then( function() {
				logger.debug( '[27]initial.js:init:fill table "t_connection_type"' );

				//
				// fill table 't_connection_type'
				//
				var bulkData = [];

				db.models.t_connection_type.findAndCountAll().then( function( result ) {
					for ( var key in initials.supported ) {
						reverseSupported[key] = initials.supported[key].order;
					}

					if ( result.count == 0 ) {
						for ( var key in initials.supported ) {
							bulkData.push( {
								id_connection_type:   initials.supported[key].order,
								connection_type_name: key
							} );
						}

						return db.models.t_connection_type.bulkCreate( bulkData );
					}
					else {
						logger.debug( '[50]initial.js:init' );

						return Q.resolve( reverseSupported );
					}
				}).then( function() {
					logger.debug( '[55]initial.js:init:fill table "t_module"' );

					//
					// fill tables 't_module' and 't_report'
					//

					var reports  = requireDir( modulesPath );
					var bulkData = [];

					for ( var key in reports ) {
						var listReports = [];

						for ( var reportName in reports[key].reports ) {
							reports[key].reports[reportName].forEach(function( reportPath ) {
								var report = require( path.join( modulesPath, reportPath ) );

								listReports.push( {
									rep:     report,
									relPath: reportPath
								} );
							} );
						}

						bulkData.push( {
							module_name:        key,
							id_connection_type: reverseSupported[ reports[key].type ],
							reports:            listReports
						} );
					}

					logger.debug( '[85]initial.js:init:fill table "t_module"' );

					return newconfig.saveModules( bulkData );
				}).then( function() {
					logger.debug( '[89]initial.js:init:set newconfig' );

					newconfig.connectionType = initials.supported;
					newconfig.reverseType    = reverseSupported;

					return Q.resolve( reverseSupported );
				}).then( function( rev ) {
					logger.debug( '[93]initial.js:init:fill table "t_group"' );

					//
					// fill table 't_group'
					//
					var serversData = serverConfig.getServersData();
					var servers     = serverConfig.getAvailable();
					var aGroupData  = [];
					var defer       = Q.defer();

					logger.debug( 'serversData: {', serversData, '}' );

					logger.debug( '[105]initial.js:init:fill table "t_group" with "No Group"' );

					//
					// TODO: very bad code
					// TODO: we need to remove 'No Group' or setupr 'No Group' for all connection types
					//

					// 1. set no group data
					aGroupData.push( {
						'id_group':           1,
						'id_connection_type': 1,
						'group_name':         'No Group',
						'is_active':          true
					} );
					aGroupData.push( {
						'id_group':           2,
						'id_connection_type': 2,
						'group_name':         'No Group',
						'is_active':          true
					} );

					logger.debug( '[115]initial.js:init:fill table "t_group" with other groups' );

					// 2. find all connections and save groups
					for ( var i = 0; i < servers.length; i++ ) {
						if ( servers[i] in serversData ) {
							logger.debug( 'servers[i]: {', servers[i], '}' );

							// check for unique group
							if ( 'groups' in serversData[servers[i]] ) {
								for ( var j = 0; j < serversData[servers[i]].groups.length; j++ ) {
									if ( findInArrayObjects( aGroupData, 'group_name', serversData[servers[i]].groups[j] ) == -1 ) {
										aGroupData.push( {
											'id_connection_type': reverseSupported[ serversData[servers[i]].type ],
											'group_name':         _.clone( serversData[servers[i]].groups[j] ),
											'is_active':          true
										} );
									}
								}
							}
						}
					}

					return db.models.t_group.findAndCountAll().then( function( result ) {
						if ( result && 'count' in result && result.count > 0 ) {
							return newconfig.updateGroups( aGroupData );
						}
						else {
							return db.models.t_group.bulkCreate( aGroupData );
						}
					} );
				} ).then( function() {
					logger.debug( '[93]initial.js:init:fill table "t_connection"' );

					//
					// fill table 't_connection'
					//
					var servers   = serverConfig.getAvailable();
					var aConnData = [];
					var aProm     = [];
					var defer     = Q.defer();
					var srvData   = null;
					var conn_type = '';

					for ( var i = 0; i < servers.length; i++ ) {
						conn_type = '';
						srvData   = serverConfig.getServersData( servers[i] );

						if ( srvData != null ) {
							conn_type = srvData.type;
						}

						logger.debug( 'servers[i]: {', servers[i], '}' );
						logger.debug( 'conn_type: {', conn_type, '}' );

						aConnData.push( {
							'serverName': servers[i],
							'conn_type':  conn_type
						} );
					}

					return db.transaction( function( t ) {
						for ( var i = 0; i < aConnData.length; i++ ) {
							aProm.push( new Promise( function( resolve, reject ) {
								var rConn = _.clone( aConnData[i] );

								logger.debug( 'rConn.conn_type: {', rConn.conn_type, '}' );
								logger.debug( 'rConn.serverName: {', rConn.serverName, '}' );

								return db.models.t_connection_type.findOrCreate( {
									where: {
										'connection_type_name': rConn.conn_type
									},
									transaction: t
								} ).spread( function( TConnectionType, created ) {
									logger.debug( 'TConnectionType: {', TConnectionType, '}' );
									logger.debug( 'created: {', created, '}' );

									logger.debug( 'rConn.serverName: {', rConn.serverName, '}' );
									logger.debug( 'rConn.conn_type: {', rConn.conn_type, '}' );
									logger.debug( 'TConnectionType.id_connection_type: {', TConnectionType.id_connection_type, '}' );

									db.models.t_connection.findOrCreate( {
										where: {
											'serverName':         rConn.serverName,
											'conn_type':          rConn.conn_type,
											'id_connection_type': TConnectionType.id_connection_type
										},
										defaults: {
											'serverName':         rConn.serverName,
											'conn_type':          rConn.conn_type,
											'id_connection_type': TConnectionType.id_connection_type
										},
										transaction: t
									} ).spread( function( TConnection, created ) {
										logger.debug( 'TConnection: {', TConnection, '}' );
										logger.debug( 'created: {', created, '}' );

										resolve( TConnection.dataValues );

										return defer.resolve();
									} );
								} );
							} ) );
						}

						logger.debug( '[231]initial.js:init' );

						return defer.promise;
					} ).then( function() {
						if ( aProm.length > 0 ) {
							return Promise.all( aProm ).then( function( aDataValues ) {
								return Q.resolve( aDataValues );
							},
							function( err ) {
								logger.error( '[209]', err );

								return Q.reject( err )
							} );
						}
					} );
				} )
				.then( function( aConnData ) {
					logger.debug( '[221]initial.js:init' );

					return newconfig.saveGroupConnections( aConnData );
				} );
			});
	};

	/*
	 * function to save modules data
	 *
	 */
	newconfig.saveModules = function( bulkData ) {
		logger.debug( '[306]newconfig.saveModules' );

		return db.models.t_module.update( {
				is_active: 0
			}, {
				where: ['1']
			}
		).then( function() {
			var index = 0;

			return promiseWhile( function() {
				return index < bulkData.length
			}, function() {
				var defer = Q.defer();

				logger.debug( '[321]index: {', index, '}' );

				db.models.t_module.findOrCreate( {
					where: {
						module_name:        bulkData[index].module_name,
						id_connection_type: bulkData[index].id_connection_type
					}
				}).spread( function( module, created ) {
					var innerD = Q.resolve();

					if ( !created ) {
						module.is_active = true;

						innerD = module.save();
					}

					return innerD.then( function() {
						return newconfig.saveQuery(
							module.id_module,
							initials.supported[module.id_connection_type],
							bulkData[index].reports
						).then( function() {
							index ++;

							defer.resolve();
						} );
					} );
				} );

				return defer.promise;
			} );
		} ).finally( function() {
			return Q.resolve();
		} );

		/*
		var index = 0;

		return promiseWhile( function() {
			return index < bulkData.length
		}, function() {
			var defer = Q.defer();

			logger.debug( '[365]index: {', index, '}' );
			logger.debug( '[366]bulkData[index].module_name: {', bulkData[index].module_name, '}' );
			logger.debug( '[367]bulkData[index].id_connection_type: {', bulkData[index].id_connection_type, '}' );

			db.models.t_module.findOrCreate( {
				where: {
					module_name:        bulkData[index].module_name,
					id_connection_type: bulkData[index].id_connection_type
				}
			}).spread( function( module, created ) {
				logger.debug( '[375]module: {', module, '}' );
				logger.debug( '[376]created: {', created, '}' );

				var innerD = Q.resolve();

				if ( !created ) {
					module.is_active = true;

					innerD = module.save();
				}

				return innerD.then( function() {
					return newconfig.saveQuery(
						module.id_module, module.id_connection_type, bulkData[index].reports
					).then( function() {
						index ++;

						defer.resolve();
					} );
				} );
			} );

			return defer.promise;
		} );
		*/
	};

	/*
	 * function to save reports data
	 *
	 */
	newconfig.saveQuery = function( id_module, /*connectionType*/id_connection_type, reports ) {
		logger.debug( '[406]newconfig.saveQuery' );
		logger.debug( '[407]id_module: {', id_module, '}' );
		logger.debug( '[408]id_connection_type: {', id_connection_type, '}' );
		logger.debug( '[409]reports: {', reports, '}' );

		var index2 = 0;

		return promiseWhile( function() {
			return index2 < reports.length
		}, function() {
			var defer2   = Q.defer();
			var relpath  = reports[index2].relPath.split( '/' );
			var fileName = '';
			var aProm    = [];

			return db.transaction( function( t ) {
				for ( var i = 0; i < reports[index2].rep.extract.length; i++ ) {
					for ( var j = 0; j < reports[index2].rep.extract[i].file.length; j++ ) {
						aProm.push( new Promise( function( resolve, reject ) {
							var inx     = _.clone( index2 );
							var file    = _.clone( reports[inx].rep.extract[i].file[j] );
							var repName = _.clone( reports[inx].rep.name );

							logger.debug( '[429]repName: {', repName, '}' );
							logger.debug( '[430]id_module: {', id_module, '}' );

							return db.models.t_report.findOrCreate( {
								where: {
									'report_name': _.clone( repName ),
									'id_module':   id_module
								},
								transaction: t
							} ).spread( function( TReport, created ) {
								logger.debug( '[306]TReport.id_report: {', TReport.id_report, '}' );
								logger.debug( '[307]created: {', created, '}' );

								db.models.t_query.findOrCreate( {
									where: {
										'query_location': path.join( reports[index2].relPath, _.clone( file.file )),
										'id_report':      TReport.id_report,
									},
									transaction: t
								} ).spread( function( TQuery, created ) {
									var innerD2 = Q.resolve();

									logger.debug( '[318]TQuery.id_query: {', TQuery.id_query, '}' );
									logger.debug( '[319]created: {', created, '}' );

									if ( !created ) {
										TQuery.is_active = true;

										innerD2 = TQuery.save();

										resolve();

										return defer2.resolve();
									}
									else {
										logger.debug( '[331]' );
										resolve();
										logger.debug( '[333]' );

										return defer2.resolve();
									}
								} );
							} );
						} ) );
					}
				}

				logger.debug( '[433]' );

				return defer2.promise;
			} )
			.then( function() {
				logger.debug( '[438]' );

				if ( aProm.length ) {
					relpath.pop();

					logger.debug( '[443]index2: {', index2, '}' );

					index2++;

					return Promise.all( aProm ).then( function( result ) {
						logger.debug( '[448]' );

						return defer2.promise;
					} );
				}
			},
			function( err ) {
				logger.error( '[455]', err );

				return defer2.reject();
			} );

			logger.debug( '[460]' );

			relpath.pop();

			logger.debug( '[464]' );

			return defer2.promise;
		} );
	};

	/*
	 * function to update groups
	 *
	 */
	newconfig.updateGroups = function( aGroupData ) {
		var aProm  = [];
		var defer2 = Q.defer();

		logger.debug( 'newconfig.updateGroups' );

		return db.transaction( function( t ) {
			for ( var i = 0; i < aGroupData.length; i++ ) {
				aProm.push( new Promise( function( resolve, reject ) {
					var rData = _.clone( aGroupData[i] );

					logger.debug( 'rData: {', rData, '}' );

					return db.models.t_group.findOrCreate( {
						where: {
							group_name: rData.group_name
						},
						defaults: {
							group_name: rData.group_name,
							is_active:  true
						},
						transaction: t
					} ).spread( function( query, created ) {
						var ret = Q.resolve();

						if ( !created ) {
							query.is_active = true;

							ret = query.save();

							resolve();

							return defer2.resolve();
						}
						else {
							resolve();

							return defer2.resolve();
						}
					} );
				} ) );
			}

			return defer2.promise;
		} )
		.then( function() {
			if ( aProm.length > 0 ) {
				return Promise.all( aProm ).then( function( res ) {
					return defer2.resolve();
				}
				,function( err ) {
					logger.error( '[391]', err );

					return defer2.reject( err );
				} );
			}
			else {
				return defer2.resolve();
			}
		}
		,function( err ) {
			logger.error( '401', err );

			return defer2.reject( err );
		} );

		return Q.resolve();
	};

	/*
	 * function save connections
	 * and groups connections
	 */
	newconfig.saveGroupConnections = function( aConnData ) {
		var defer2 = Q.defer();
		var aProm  = [];

		logger.debug( 'newconfig.saveGroupConnections' );

		if ( !aConnData ) {
			return Q.reject( {
				error: 'No Data Connection!'
			} );
		}

		return db.models.t_group.findAndCountAll().then( function( data ) {
			var srvData     = null;
			var aCreateData = [];

			if ( !data || !data.count ) {
				return defer2.reject();
			}

			for ( var i = 0; i < aConnData.length; i++ ) {
				srvData = serverConfig.getServersData( aConnData[i].serverName );

				logger.debug( 'srvData: {', srvData, '}' );

				if ( srvData != null ) {
					if ( !( 'groups' in srvData ) ) {
						logger.debug( 'aConnData[i].id_connection: {', aConnData[i].id_connection, '}' );

						aCreateData.push( {
							id_group:      1,
							id_connection: aConnData[i].id_connection
						} );
					}
					else {
						// find in all groups
						for ( var j = 0; j < data.count; j++ ) {
							if ( srvData.groups.indexOf( data.rows[j].group_name ) != -1 ) {
								logger.debug( 'data.rows[j].id_group: {', data.rows[j].id_group, '}' );
								logger.debug( 'aConnData[i].id_connection: {', aConnData[i].id_connection, '}' );

								aCreateData.push( {
									id_group:      data.rows[j].id_group,
									id_connection: aConnData[i].id_connection
								} );
							}
						}
					}
				}
			}

			return Q.resolve( aCreateData );
		} )
		.then( function( aData ) {
			// destroy data and create it
			return db.models.t_connection_group.destroy( {
				where: {
				}
			} )
			.then( function() {
				if ( aData.length ) {
					logger.debug( 'main.sqlite initialize - Ok');

					return db.models.t_connection_group.bulkCreate( aData );
				}
				else {
					return defer2.resolve();
				}
			},
			function( err ) {
				logger.error( '[503]', err );

				return Q.reject( err );
			} );
		} );
	};

	/*
	 * function search value
	 * in array of objects
	 */
	function findInArrayObjects( arr, key, value ) {
		var ret = -1;

		if ( !arr || !key ) {
			return ret;
		}

		for ( var i = 0; i < arr.length; i++ ) {
			if ( key in arr[i] ) {
				if ( arr[i][key] == value ) {
					ret = i;
					break;
				}
			}
		}

		return ret;
	}

	module.exports = {
		newconfig: newconfig,
		init:      init
	}
} )( module );
