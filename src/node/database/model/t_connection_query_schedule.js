'use strict';

(function (module) {
	var Promise               = require( 'promise' );
	var Sequelize             = require( 'sequelize' );
	var _                     = require( 'lodash' );
	var main_storage_database = require( 'src/node/database' )();
	var t_report              = require( './t_report' );
	var t_module              = require( './t_module' );
	var t_group               = require( './t_group' );
	var t_connection          = require( './t_connection' );
	var t_connection_type     = require( './t_connection_type' );
	var logger                = require( 'src/node/log' );
	var nodeSchedule          = require( 'node-schedule' );

	var TConnectionQuerySchedule = main_storage_database.define( 't_connection_query_schedule', {
		// column 'id_connection_query_schedule'
		id_connection_query_schedule: {
			field:         'id_connection_query_schedule',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'schedule identificator - primary key',
			validate:      {
			}
		},

		// column 'id_connection'
		id_connection: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_connection,

				// This is the column name of the referenced model
				key: 'id_connection'
			}
		},

		// column 'id_group'
		id_group: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_group,

				// This is the column name of the referenced model
				key: 'id_group'
			}
		},

		// column 'id_report'
		id_report: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_report,

				// This is the column name of the referenced model
				key: 'id_report'
			}
		},

		// column 'forGroup'
		forGroup: {
			field:        'forGroup',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is execution schedule for the group of connections',
			validate:     {
			}
		},

		// column 'schedule'
		schedule: {
			field:     'schedule',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'execution schedule, like {30 * * * * *} - execute report every minute at 30 second',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'isEnabled'
		isEnabled: {
			field:        'isEnabled',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is schedule enabled',
			validate:     {
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection_query_schedule',

		comment: 'schedule query for the single server (connection)',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TConnectionQuerySchedule.belongsTo(
				models.t_report, {
					foreignKey: 'id_report'
				}
			);

			TConnectionQuerySchedule.belongsTo(
				models.t_connection, {
					foreignKey: 'id_connection'
				}
			);

			TConnectionQuerySchedule.belongsTo(
				models.t_group, {
					foreignKey: 'id_group'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_connection_query_schedule',
			unique: true,
			fields: [
				'id_connection',
				'id_group',
				'id_report',
				'forGroup',
				'schedule'
			]
		} ]
	} );

	TConnectionQuerySchedule.sync( {
		force: false
	} ).then( function() {
		logger.debug( '[156]TConnectionQuerySchedule.sync' );
	} );

	/*
	TConnectionQuerySchedule.sync({ force: false }).then( function() {
		logger.debug( '[154]TConnectionQuerySchedule.sync' );

		var t_connection   = require( './t_connection' );
		var promises       = [];
		var Report         = require( 'src/node/com/report' )( 'start' );
		var activeSchedule = null;
		var jobName        = null;

		TConnectionQuerySchedule.findAll( {
			where: {
				isEnabled: true
			},

			// include: [ {
			// 	model: t_report,
			// 	include: [ {
			// 		model: t_module,
			// 		include: [ {
			// 			model: t_connection_type
			// 		} ]
			// 	} ]
			// } ]

			// include: [ {
			// 	model: t_report, as: 'tR',
            //
			// 	include: [ {
			// 		model: t_module, as: 'tM'
			// 	} ]
			// }, {
			// 	model: t_connection, as: 'tC',
            //
			// 	include: [ {
			// 		model: t_connection_type, as: 'tCTC'
			// 	} ]
			// }, {
			// 	model: t_group, as: 'tG',
            //
			// 	include: [ {
			// 		model: t_connection_type, as: 'tCTG'
			// 	} ]
			// } ]

			include: [ {
				model: t_connection, as: 'tC'
			} ]
		} ).then( function( result ) {
			logger.debug( '[194]result: {', result, '}' );

			result.forEach( function( item ) {
				logger.debug( '[197]item: {', item, '}' );
				logger.debug( '[198]item.isEnabled: {', item.isEnabled, '}' );

				if ( !item.isEnabled ) {
					return;
				}

				item.repName    = item.t_report.report_name;
				item.moduleType = item.t_report.t_module.module_name;
				item.connType   = item.t_connection.t_connection_type.connection_type_name;
				item.connName   = item.t_connection.serverName;

				logger.debug( '[209]item.repName: {', item.repName, '}' );
				logger.debug( '[210]item.moduleType: {', item.moduleType, '}' );
				logger.debug( '[211]item.connType: {', item.connType, '}' );
				logger.debug( '[212]item.connName: {', item.connName, '}' );

				logger.debug( '[214]item.forGroup: {', item.forGroup, '}' );

				if( !item.forGroup ) {
					promises.push( new Promise( function( resolve, reject ) {

					jobName = getCurrNameJob(item);

					logger.debug( '[221]jobName: {', jobName, '}' );

					activeSchedule = nodeSchedule.scheduleJob( jobName, item.schedule, function() {
						Report.getReportByScheduler( item.repName, item.connType, item.connName, item.moduleType, true )
							.then(data => {
								return resolve( data );
							} )
							.catch( err => {
								logger.error( '[229]', err );

								return reject( err );
							} )
						} );
					} ) );
				}
				else {
					jobName = getCurrNameJob( item );

					logger.debug( '[239]jobName: {', jobName, '}' );

					database.models.t_group.findOne( {
						where: {
							group_name: item.groupName
						}
					} ).then( function( rData ) {
						database.models.t_connection_group.findAndCountAll( {
							where: {
								id_group: rData.id_group
							},
							include: [ {
								model: database.models.t_connection
							} ]
						} ).then( function( aData ) {
							if( aData.count ){
								for( var i = 0; i < aData.count; i++ ){
									promises.push( new Promise( function( resolve, reject ){
										var connName2 = _.clone( aData.rows[i].dataValues.t_connection.dataValues.serverName );

										active_schedule = nodeSchedule.scheduleJob( jobName, item.schedule, function() {
											Report.getReportByScheduler( item.repName, item.connType, connName2, item.moduleType, true )
												.then( function( repData ) {
													resolve( repData );
												},
												function( err ) {
													logger.error( '[220]', err );

													reject( err );
												} );
											});
									} ) );
								}
							}
							else {
								return dfd.reject();
							}
						} );
					} );
				}
			});
		})
		.catch( function( err ) {
			logger.error( '[285]', err );
			logger.error( '[286]TConnectionQuerySchedule' );
		})
		.finally(function() {
		});
	});
	*/

	function getCurrNameJob( record ) {
		return record.connType + record.connName + record.moduleType + record.repName + record.schedule;
	};

	function extractItem( item ) {
		var dataValues = item.dataValues;
		var resItem    = {};

		for ( var key in dataValues ) {
			if ( dataValues.hasOwnProperty(key) ) {
				resItem[key] = dataValues[key];
			}
		}

		return resItem;
	};

	module.exports = TConnectionQuerySchedule;
} )( module );
