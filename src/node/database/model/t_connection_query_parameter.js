'use strict';

(function (module) {
	var Sequelize         = require( 'sequelize' );
	var database          = require( 'src/node/database' )();
	var t_query_parameter = require( './t_query_parameter' );
	var t_connection      = require( './t_connection' );

	var TConnectionQueryParameter = database.define( 't_connection_query_parameter', {
		// column 'connection_query_parameter_id'
		connection_query_parameter_id: {
			field:         'connection_query_parameter_id',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'connection query parameter identificator - primary key',
			validate:      {
			}
		},

		// column 'query_parameter_value'
		query_parameter_value: {
			field:     'query_parameter_value',
			type:      Sequelize.STRING,
			allowNull: true,
			comment:   'query parameter value',
			validate:  {
			}
		},

		// column 'id_connection'
		id_connection: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_connection,

				// This is the column name of the referenced model
				key: 'id_connection'
			}
		},

		// column 'id_query_parameter'
		id_query_parameter: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_query_parameter,

				// This is the column name of the referenced model
				key: 'id_query_parameter'
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection_query_parameter',

		comment: 'connection (server) query parameter',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true
	} );

	module.exports = TConnectionQueryParameter;
})(module);
