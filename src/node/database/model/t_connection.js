'use strict';

(function (module) {
	var Sequelize                   = require( 'sequelize' );
	var default_database            = require( 'src/node/database' )();
	var t_connection_type           = require( './t_connection_type' );
	var t_connection_query_schedule = require( './t_connection_query_schedule' );

	var TConnection = module.exports = default_database.define( 't_connection', {
		// column 'id_connection'
		id_connection: {
			field:         'id_connection',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'connection identificator - primary key',
			validate:      {
			}
		},

		// column 'serverName'
		serverName: {
			field:     'serverName',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'external server name as the data source (unique)',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'conn_type'
		conn_type: {
			field:     'conn_type',
			type:      Sequelize.STRING,
			allowNull: true,
			comment:   'type of group connections',
			validate:  {
			}
		},

		// column 'is_active'
		is_active: {
			field:        'is_active',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: true,
			comment:      'is connection active (exists)',
			validate:     {
			}
		},

		// column 'id_connection_type'
		id_connection_type: {
			type: Sequelize.INTEGER,

			references: {
				// it is the reference to another model
				model: t_connection_type,
				// it is the column name of the referenced model
				key: 'id_connection_type'
			}
		}
	}, {
		// define the table's name
		tableName: 't_connection',

		comment: 'list of connections to external data sources',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TConnection.belongsTo(
				models.t_connection_type, {
					foreignKey: 'id_connection_type'
				}
			);

			TConnection.hasMany(
				models.t_connection_query_schedule, {
					foreignKey: 'id_connection'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_connection_serverName',
			unique: true,
			fields: [
				'id_connection_type',
				'serverName'
			]
		} ]
	} );

	module.exports = TConnection;
} )( module );
