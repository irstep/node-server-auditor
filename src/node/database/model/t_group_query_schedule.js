'use strict';

( function( module ) {
	var Sequelize             = require( 'sequelize' );
	var main_storage_database = require( 'src/node/database' )();
	var t_group               = require( './t_group' );
	var t_report              = require( './t_report' );

	var TGroupQuerySchedule = main_storage_database.define( 't_group_query_schedule', {
		// column 'group_query_schedule_id'
		group_query_schedule_id: {
			field:         'group_query_schedule_id',
			type:          Sequelize.INTEGER,
			allowNull:     false,
			primaryKey:    true,
			autoIncrement: true,
			comment:       'group query schedule identificator - primary key'
		},

		// column 'id_group'
		id_group: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_group,

				// This is the column name of the referenced model
				key: 'id_group'
			}
		},

		// column 'id_report'
		id_report: {
			type: Sequelize.INTEGER,

			references: {
				// This is a reference to another model
				model: t_report,

				// This is the column name of the referenced model
				key: 'id_report'
			}
		},

		// column 'schedule'
		schedule: {
			field:     'schedule',
			type:      Sequelize.STRING,
			allowNull: false,
			comment:   'execution schedule, like {30 * * * * *} - execute report every minute at 30 second',
			validate:  {
				notEmpty: true // don't allow empty strings
			}
		},

		// column 'isEnabled'
		isEnabled: {
			field:        'isEnabled',
			type:         Sequelize.BOOLEAN,
			allowNull:    false,
			defaultValue: false,
			comment:      'is execution schedule enabled',
			validate:     {
			}
		}
	}, {
		// define the table's name
		tableName: 't_group_query_schedule',

		comment: 'schedule query for the group of servers (connections)',

		// 'createdAt' to actually be called '_datetime_created'
		createdAt: '_datetime_created',

		// 'updatedAt' to actually be called '_datetime_updated'
		updatedAt: '_datetime_updated',

		// disable the modification of table names; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural.
		freezeTableName: true,

		charset: 'utf8',

		underscored: true,

		associate: function( models ) {
			TGroupQuerySchedule.belongsTo(
				models.t_group, {
					foreignKey: 'id_group'
				}
			);

			TGroupQuerySchedule.belongsTo(
				models.t_report, {
					foreignKey: 'id_report'
				}
			);
		},

		indexes: [ {
			name:   'uidx_t_group_query_schedule',
			unique: true,
			fields: [
				'id_group',
				'id_report',
				'schedule'
			]
		} ]
	} );

	module.exports = TGroupQuerySchedule;
} )( module );
