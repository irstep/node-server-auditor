/* global angular, app, $, window */

/**
 * @link source https://github.com/angular-ui/ui-chart
 */
app.directive('uiChart', ['$timeout', function ($timeout) {
	return {
		restrict: 'EACM',
		template: '<div></div>',
		replace: true,
		scope: true,
		link: function (scope, elem, attrs) {
			var resizeLock = false, renderChart, resize, onResizeFn;

			renderChart = function() {
				var data = scope.$eval(attrs.uiChart), opts = {};

				elem.html('');

				if (!angular.isArray(data)) {
					return;
				}

				if (!angular.isUndefined(attrs.chartOptions)) {
					opts = scope.$eval(attrs.chartOptions);

					if (!angular.isObject(opts)) {
						throw 'Invalid ui.chart options attribute';
					}
				}

				$timeout( function() {
					resize();

					elem.jqplot(data, opts);
				}, 0);
			};

			resize = function () {
				var margin = 20;

				elem.height($(window).height() - elem.offset().top - margin);
				elem.width($(window).width() - elem.offset().left - margin);
			};

			onResizeFn = function () {
				var resizeTimeout = 500; // ms

				if ( !resizeLock ) {
					resizeLock = true;

					$timeout( function() {
						resizeLock = false;

						renderChart();
					}, resizeTimeout);
				}
			};

			scope.$watch(attrs.uiChart, renderChart, true);
			scope.$watch(attrs.chartOptions, renderChart, true);

			scope.$on('replot.uiChart', function (evt, options) {
				if (elem.is(':visible') && elem.data('jqplot')) {
					resize();

					elem.data('jqplot').replot();
				}
			});

			$(window).on('resize', onResizeFn);

			scope.$on('$destroy', function() {
				$(window).off('resize', onResizeFn);
			});
		}
	};
}]);
