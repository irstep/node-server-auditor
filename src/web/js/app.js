var app = angular.module('nodeServerAuditor', [
		'ngRoute',
		'ngCookies',
		'ngAnimate',
		'pascalprecht.translate',
		'ngSanitize',
		'nya.bootstrap.select',
		'ui.bootstrap',
		'angular.filter'
	])
	.config(['$routeProvider', '$locationProvider', '$translateProvider', '$animateProvider',
		function ($routeProvider, $locationProvider, $translateProvider, $animateProvider) {
			$routeProvider
				.when('/', {
					templateUrl: '/pages/auth.html',
					controller:  'authCtrl',
					activetab:   'auth'
				})
				.when('/reports/:name', {
					templateUrl: '/pages/report.html',
					controller:  'reportCtrl',
					activetab:   'reports'
				})
				.when('/scheduler', {
					templateUrl: '/pages/scheduler.html',
					controller:  'schedulerCtrl',
					activetab:   'scheduler'
				})
				.otherwise( {
					redirectTo: '/'
				} );

			$locationProvider.html5Mode(true);

			$translateProvider.useLoader('langLoader');
			$translateProvider.preferredLanguage('en');
			$translateProvider.useCookieStorage();
			$translateProvider.useSanitizeValueStrategy('escape');

			$animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
	}]);
