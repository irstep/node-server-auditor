/**
 * Date Line Plain Directive
 *
 * @requires baseGraphLink
 * @requires DateLinePlainComp
 */
app.directive('dateLinePlainReport', ['baseGraphLink', 'DateLinePlainComp', function (link, DateLinePlainComp) {
	return {
		restrict: 'EA',

		templateUrl: 'directives/report/graphs/default-graph.html',

		scope: {
			reportItem: '=dateLinePlainReport'
		},

		link: function ($scope, element, attrs) {
			link($scope, element, attrs, DateLinePlainComp);
		}
	};
}]);
