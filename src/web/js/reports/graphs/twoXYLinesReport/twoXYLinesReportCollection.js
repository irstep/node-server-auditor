/* global angular, app */

/**
 * Two XY Lines Collection Model
 *
 * @requires Data
 */
app.factory('TwoXYLinesCollection', ['Data', function (Data) {
	var TwoXYLinesCollection = function (Class, items) {
		this.rows = [];

		items.forEach(function (item) {
			var line = item.line - 1;

			if (!this.rows[line]) {
				this.rows[line] = [];
			}

			this.rows[line].push(new Class(item));
		}.bind(this));
	};

	angular.extend(TwoXYLinesCollection.prototype, Data.prototype, {
		data: function() {
			return this.rows.map( function( row ) {
				return row.map( function( item ) {
					return item.data();
				});
			});
		}
	});

	return TwoXYLinesCollection;
}]);
