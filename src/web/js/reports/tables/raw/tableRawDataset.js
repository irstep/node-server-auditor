/* global angular, app */
/**
 * Table Raw Dataset Model
 *
 * @requires Data
 */
app.factory('TableRawDataset', ['Data', function (Data) {
	var TableRawDataset = function (items) {
		this.items = items;
	};

	angular.extend(TableRawDataset.prototype, Data.prototype, {
		_getHeaders: function() {
			var items  = this.items;
			var keys   = [];
			var struct = items.struct;

			if (items[0]) {
				keys = Object.keys(items[0]);
			}

			return keys.map(function (item) {
				var result = {
					value: item
				};

				if ( struct && struct[item] ) {
					$.extend(result, struct[item]);
				}

				result.displayName = result.displayName || result.value;

				result.style = {
					'color':            result.color      ? result.color      : null,
					'background-color': result.bgcolor    ? result.bgcolor    : null,
					'text-align':       result.alignment  ? result.alignment  : null,
					'vertical-align':   result.valignment ? result.valignment : null,
				};

				return result;
			});
		},

		_getValues: function() {
			var items = this.items;

			return items.map(function (item) {
				var result = [];

				for (var key in item) {
					if (item.hasOwnProperty(key)) {
						result.push(item[key]);
					}
				}

				return result;
			});
		},

		data: function() {
			return {
				headers: this._getHeaders(),
				rows:    this._getValues()
			};
		}
	});

	return TableRawDataset;
}]);
