/* global angular, app */

app.factory('Data', function() {
	var Data = function() {
	};

	Data.prototype.data = function() {
		var result = {};

		for ( var key in this ) {
			if ( this.hasOwnProperty(key) && key !== '$$hashKey' ) {
				result[key] = this[key];
			}
		}

		return result;
	};

	return Data;
});
