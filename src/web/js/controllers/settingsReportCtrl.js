(function (app) {
	app.controller('settingsReportCtrl', ['$scope', '$query', '$routeParams', '$uibModalInstance', function ($scope, $query, $routeParams, $uibModalInstance) {
		$scope.tabs         = [];
		$scope.simpleReport = true;

		$scope.loadInfoRep =  function() {
			$query('paramsrep/' + $routeParams.name ).then( function( res ) {
				var i    = 0;
				var data = res.response;

					if ( res.length !== 0 ) {
						//check, what report was built
						if ( 'extract' in data ) {
							$scope.simpleReport = false;
							$scope.paragraph    = null;
							$scope.section      = null;
							$scope.paramData    = [];
							$scope.rawData      = data;
							$scope.oldPar       = null;
							$scope.oldSect      = null;

							// extract
							for ( i = 0; i < data.extract.length; i++ ) {
								$scope.paramData.push( { paragraph: 'Extract', sectionName: data.extract[i].sectionName } );
							}

							// transform
							for ( i = 0; i < data.transformation.length; i++ ) {
								$scope.paramData.push( { paragraph: 'Transformation', sectionName: data.transformation[i].sectionName } );
							}

							// load
							for ( i = 0; i < data.load.length; i++ ) {
								$scope.paramData.push( { paragraph: 'Load', sectionName: data.load[i].sectionName } );
							}

							//default params
							$scope.paragraph = $scope.paramData[0].paragraph;
							$scope.section = $scope.paramData[0].sectionName;
							$scope.selectSection(); //draw first params
						}
						else {
							$scope.simpleReport = true;

							data.forEach(function (records) {
								$scope.tabs.push(records);
							});
						}
					}
				},
				function (err) {
					// console.log('err - ', err);
			});
		}

		$scope.save = function() {
			var i     = 0;
			var j     = 0;
			var k     = 0;
			var rSave = {};
			var aSave = [];
			var aPar  = ['extract', 'transformation', 'load'];

			if ( $scope.simpleReport ) {
				aSave = $scope.tabs;
			}
			else {
				// store data from dialog
				if ( $scope.oldPar != null ) {
					$scope.oldPar = $scope.oldPar.toLowerCase();

					if ( $scope.oldPar in $scope.rawData ) {
						for ( i = 0; i < $scope.rawData[$scope.oldPar].length; i++ ) {
							if ( $scope.rawData[$scope.oldPar][i].sectionName == $scope.oldSect ) {
								$scope.rawData[$scope.oldPar][i].params = $scope.tabs;
								break;
							}
						}
					}
				}

				// make save array
				for ( k = 0; k < aPar.length; k++ ) {
					for ( i = 0; i < $scope.rawData[aPar[k]].length; i++ ) {
						for ( j = 0; j < $scope.rawData[aPar[k]][i].params.length; j++ ) {
							rSave = $scope.rawData[aPar[k]][i].params[j];
							//add data for paragraph and section
							rSave.paragraphName = aPar[k];
							rSave.sectionName = $scope.rawData[aPar[k]][i].sectionName;
							aSave.push( rSave );
						}
					}
				}
			}

			$query( 'paramsrep/save', aSave, 'PUT' ).then( function( data ) {
				console.log( 'success: {', data, '}' );
				$uibModalInstance.close( 'all' );
			},function( res ) {
				console.log( 'res: {', res, '}' );
			});
		};

		$scope.close = function() {
			$uibModalInstance.dismiss('close');
		};

		$scope.selectSection = function() {
			var par = '';

			$scope.tabs = [];

			// save changes in params
			if ( $scope.oldPar != null ) {
				$scope.oldPar = $scope.oldPar.toLowerCase();

				if ( $scope.oldPar in $scope.rawData ) {
					for ( i = 0; i < $scope.rawData[$scope.oldPar].length; i++ ) {
						if ( $scope.rawData[$scope.oldPar][i].sectionName == $scope.oldSect ) {
							$scope.rawData[$scope.oldPar][i].params = $scope.tabs;
							break;
						}
					}
				}

				$scope.oldPar  = $scope.paragraph;
				$scope.oldSect = $scope.section;
			}

			par = $scope.paragraph.toLowerCase();

			if ( par in $scope.rawData ) {
				for ( i = 0; i < $scope.rawData[par].length; i++ ) {
					if ( $scope.rawData[par][i].sectionName == $scope.section ) {
						$scope.tabs = $scope.rawData[par][i].params;
						break;
					}
				}
			}
		};
	}]);
})(app);
