(function (app) {
	app.controller('authCtrl', ['$scope', '$query', 'alertify', function ($scope, $query, alertify) {
		var setConf;
		var getTypeByModuleName;

		$scope.modules = [];
		$scope.servers = [];
		$scope.grupped = [];
		$scope.group   = null;
		$scope.dbType;
		$scope.moduleName;
		$scope.server  = null;

		setConf = function (config) {
			$query('config/reports', config, 'PUT').then( function() {
				alertify('success', 'alertConfigSaved');

				$scope.$emit('nsaConfigUpdated', config);
			}, function() {
				alertify( 'error', 'alertNetworkError' );
			});
		};

		getTypeByModuleName = function (name) {
			for ( var i = 0; i < $scope.modules.length; i++ ) {
				if ( $scope.modules[i].name === name ) {
					return $scope.modules[i].type;
				}
			}
		};

		$scope.submit = function () {
			var config = {
				server: $scope.server,
				module: $scope.moduleName,
				group:  $scope.group
			};

			setConf(config);
		};

		$scope.deleteSettings = function () {
			setConf(null);
		};

		$query('config/reports').then(function (data) {
			var res          = data.response;
			var isConfigured = res.config !== null;

			$scope.modules = res.modules;
			$scope.servers = res.servers;
			$scope.grupped = res.grupped;

			if ( isConfigured ) {
				$scope.dbType     = getTypeByModuleName(data.response.config.module);
				$scope.moduleName = data.response.config.module;
				$scope.server     = data.response.config.server.name;
				$scope.group      = data.response.config.server.group;
			}
			else {
				$scope.dbType     = $scope.modules[0].type;
				$scope.moduleName = $scope.modules[0].name;
				$scope.group      = $scope.grupped[0].t_group.group_name;
				$scope.server     = $scope.grupped[0].t_connection.serverName;

				$scope.changeServerType( $scope.dbType );

				alertify('error', 'alertNotConfiguredError');
			}
		}, function() {
		});

		$scope.changeServerType = function( dbType ) {
			var i = 0;

			$scope.server = ($scope.grupped[0].t_connection.conn_type == dbType) ? $scope.grupped[0].t_connection.serverName : null;
			$scope.group  = $scope.grupped[0].t_group.group_name;
		}
	}]);
})(app);
