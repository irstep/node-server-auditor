(function (app) {
	app.controller('bodyCtrl', ['$scope', '$query', '$location', 'tabsCollectionClass', '$uibModal',
		function ($scope, $query, $location, TabsCollection, $uibModal) {
			$scope.mainTabs          = new TabsCollection(/(\w+)\//);
			$scope.reportsCollection = new TabsCollection(/reports\/(\w+)/);

			$scope.updateReports = function() {
				$query('reports').then(function (res) {
					$scope.reportsCollection.clear();

					res.response.forEach(function(report) {
						$scope.reportsCollection.add({
							name: report
						});
					});

				}, function() {
					$scope.reportsCollection.clear();
				});
			};

			$scope.$on('$routeChangeSuccess', function (evt, $route) {
				var tab = [$route.$$route.activetab];

				for (var key in $route.params) {
					if ($route.params.hasOwnProperty(key)) {
						tab.push($route.params[key]);
					}
				}

				$scope.activeRoute = tab.join('/');

				$scope.mainTabs.setActiveByRoute($scope.activeRoute);
				$scope.reportsCollection.setActiveByRoute($scope.activeRoute);
			});

			$scope.$on('nsaConfigUpdated', function (event, data) {
				$scope.updateReports();
			});
			
			$scope.checkIsReportsActive = function() {
				return $location.url().indexOf('reports') > -1;
			}
			
			$scope.onReloadReportData = function() {
				$scope.$broadcast('onReloadReportData');
			}			
			
			$scope.onRefreshReportData = function() {
				$scope.$broadcast('onRefreshReportData');
			}			
			
			$scope.onShowReportInfo = function() {
				$scope.$broadcast('onShowReportInfo');
			}			
									
			$scope.route = function(path) {
				$location.path(path);
			}

			$scope.routeActiveReport = function() {
				var active = $scope.reportsCollection.getActive();
				// if ( active ) {
				//    $scope.route('/reports/' + active.name);
				// }
			}

			$scope.openSettingsModal = function (size) {
				$uibModal.open({
					animation:   true,
					templateUrl: '/pages/settings-modal.html',
					controller:  'settingsModalCtrl'
				});
			};

			$scope.openSettingsReport = function (size) {
				// console.log( 'openSettingsReport' );

				$uibModal.open({
					animation:   true,
					templateUrl: '/pages/settings-reports.html',
					controller:  'settingsReportCtrl'
				});
			};

			$scope.loadMenu = function() {
				$query('reports/json').then(function (res) {
					$scope.pars = res.response;
				}, function() {
				});
			}

			$scope.loadMenu();
			$scope.updateReports();
			$scope.routeActiveReport();
		}]);
})(app);
