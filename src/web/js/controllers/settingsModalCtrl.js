(function (app) {
	app.controller('settingsModalCtrl', ['$scope', '$query', '$translate', '$uibModalInstance', function ($scope, $query, $translate, $uibModalInstance) {
		$query('languages').then(function (res) {
			$scope.languages = res.response;
		}, function() {
			$scope.languages = false;
		});

		$scope.current = $translate.use();

		$scope.changeLang = function (lang) {
			$translate.use(lang);

			$scope.current = lang;
		};

		$scope.close = function() {
			$uibModalInstance.dismiss('close');
		};
	}]);
})(app);
